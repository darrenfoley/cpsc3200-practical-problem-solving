#include <iostream>
#include <set>
using namespace std;

int main()
{
   set<int> s;
   s.insert(55);
   s.erase(10);
   for(auto i :s)
      cout << i  << ' ';
   cout <<endl;
   return 0;
}
