/*
  General Approach: We try all valid ways of partitioning the string into
  slimps and slumps to see if we can formulate the given string into 2 parts
  corresponding to a valid slimp + slump combination.

  I used dynamic programming, since we may end up looking at the same partition
  of the string multiple times to see if, for example, it's a slump.  This way
  we don't have to recompute things we have computed previously.  This wasn't
  really necessary since the input strings were relatively short in length,
  but I thought "Hey, why not?"
 */

#include <iostream>

using namespace std;

int slumpDP[60][60];
int slimpDP[60][60];

string str;

//must be at least 3 chars
bool slump(int s, int e)
{
   //check if we've computed it before
   if(slumpDP[s][e] == 1)
      return true;
   if(slumpDP[s][e] == 0)
      return false;

   //check starting character and at least 1 F
   if(str[s] != 'D' && str[s] != 'E' || str[s+1] != 'F')
   {
      slumpDP[s][e] = 0;
      return false;
   }

   if(str[e] != 'G')
   {
      slumpDP[s][e] = 0;
      return false;
   }
   
   for(int i = s + 2; i < e; ++i)
   {
      if(str[i] == 'D' || str[i] == 'E')
      {
	 if(slump(i, e))
	 {
	    slumpDP[s][e] = 1;
	    return true;
	 }
	 else
	 {
	    slumpDP[s][e] = 0;
	    return false;
	 }
      }
      if(str[i] != 'F')
      {
	 slumpDP[s][e] = 0;
	 return false;
      }
   }

   slumpDP[s][e] = 1;
   return true;
}

//must be at least 2 chars
bool slimp(int s, int e)
{
   //check if we've computed it before
   if(slimpDP[s][e] == 1)
      return true;
   if(slimpDP[s][e] == 0)
      return false;
   
   //check starting character
   if(str[s] != 'A')
   {
      slimpDP[s][e] = 0;
      return false;
   }
   
   //length 2 slimp case
   if(e - s == 1)
   {
      if(str[e] == 'H')
      {
	 slimpDP[s][e] = 1;
	 return true;
      }
      else
      {
	 slimpDP[s][e] = 0;
	 return false;
      }
   }

   //check for C at end
   if(str[e] != 'C')
   {
      slimpDP[s][e] = 0;
      return false;
   }
   
   //A followed by a Slump followed by a C case
   if(slump(s+1,e-1))
   {
      slimpDP[s][e] = 1;
      return true;
   }

   //A followed by B followed by a slimp followed by a C case
   if(str[s+1] == 'B' && slimp(s+2,e-1))
   {
      slimpDP[s][e] = 1;
      return true;
   }

   slimpDP[s][e] = 0;
   return false;
}

bool slurpy()
{
   //if we don't have enough characters to form a slump + a slimp
   if(str.length() < 5)
      return false;

   //otherwise we try splitting at each valid position into a slimp and a slump
   //and check if each of them is valid
   for(int i = 2; i < str.length()-2; ++i)
      if(slimp(0, i - 1) && slump(i, str.length()-1))
	 return true;

   return false;
}

int main()
{
   int N;
   cin >> N;
   cout << "SLURPYS OUTPUT\n";
   for(int i = 0; i < N; ++i)
   {
      fill(&slimpDP[0][0], &slimpDP[0][0] + sizeof(slimpDP)/sizeof(int), -1);
      fill(&slumpDP[0][0], &slumpDP[0][0] + sizeof(slumpDP)/sizeof(int), -1);
      
      cin >> str;
      if(slurpy())
	 cout << "YES\n";
      else
	 cout << "NO\n";
   }
   cout << "END OF OUTPUT\n";
   return 0;
}
