/*
  General Approach: This is a longest common substring problem.  We use the
  following recurrance:

  Base case: f(i,j), where i<0 || j<0, = 0
  
  f(i,j), where s[i] == t[j], = 1 + f(i-1,j-1)
  f(i,j), where s[i] != t[j], = 0 + max(f(i-1,j), f(i,j-1))

  We use also dynamic programming because we will end up with the same i and j
  values multiple times and there is no point in recomputing them.
*/
  
#include <iostream>
#include <algorithm>

using namespace std;

int DP[100][100];

int lcs(const string &s, const string &t, int i, int j)
{
   //base case
   if(i<0 || j<0)
      return 0;

   //check if we've seen it before
   if(DP[i][j] != -1)
      return DP[i][j];
   
   //s[i] == t[j] case
   if(s[i] == t[j])
      return DP[i][j] = 1 + lcs(s, t, i-1, j-1);

   //otherwise
   return DP[i][j] = max(lcs(s, t, i-1, j), lcs(s, t, i, j-1));
}

int main()
{
   string seq1, seq2;
   int caseNo = 1;
   getline(cin,seq1);
   while(seq1[0] != '#')
   {
      fill(&DP[0][0], &DP[0][0] + sizeof(DP)/sizeof(int), -1);
      getline(cin,seq2);
      
      cout << "Case #" << caseNo++ << ": you can visit at most "
	   << lcs(seq1,seq2, seq1.length() - 1, seq2.length() - 1)
	   << " cities.\n";

      getline(cin,seq1);
   }
   
   return 0;
}
