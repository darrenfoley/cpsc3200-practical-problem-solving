/*
  General Approach: store input in vector of Jobs (struct containing order of
  input, job time and penalty).
  Use custom compare function which puts job i before j if
  Ti / Si < Tj / Sj (or if they are equal puts them in the order they appeared
  in the input).  This works intuitively before we want jobs that take a long
  time but have a small penalty to come later than jobs that don't take a long
  time but have a large penalty.  To avoid rounding errors we rearrange the
  inequality to the form Ti * Sj < Tj * Si.  This compare function is used
  in conjunction with STL sort.
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Job
{
   Job(int _order, int _t, int _s): order(_order), t(_t), s(_s) {}
   int order, t, s;
};

bool compare(Job first, Job second)
{
   int left = first.t * second.s, right = second.t * first.s;
   //if left==right, use lexicographic ordering
   if(left == right)
      return first.order < second.order;
   //otherwise choose job first over second if left < right
   return left < right ;
}

int main()
{
   int numCases, n, t, s;
   //input
   cin >> numCases;
   for(int i = 0; i < numCases; ++i)
   {
      vector<Job> jobs;
      cin >> n;

      for(int j = 0; j < n; ++j)
      {
	 cin >> t >> s;
	 jobs.push_back(Job(j+1, t, s));
      }

      //prioritize jobs
      sort(jobs.begin(), jobs.end(), compare);

      //output
      for(int j = 0; j < n; ++j)
      {
	 cout << jobs[j].order;
	 if(j == n-1)
	    cout << endl;
	 else
	    cout << ' ';
      }
      if(i != numCases - 1)
	 cout << endl;
   }
   return 0;
}
