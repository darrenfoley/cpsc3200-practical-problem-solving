/*
  General Approach: include all input in the ouput subsequence except 0's,
  unless the input only consists of 1 or more 0's, then just output a single 0.
  This works because 0's do not contribute to the sum, only to the length
  (and we want the shortest non-zero length).
*/

#include <iostream>
#include <vector>

using namespace std;

int main()
{
   int n, current;
   cin >> n;
   while(n != 0)
   {
      vector<int> subseq;
      for(int i = 0; i < n; ++i)
      {
	 cin >> current;
	 if(current)
	    subseq.push_back(current);
      }
      /*
	usually we want to ignore 0's, but if the only integers in the input
	were 0's we should print one 0.
      */
      if(subseq.size() == 0)
	 cout << '0';
      else
	 for(int i = 0; i < subseq.size(); ++i)
	 {
	    cout << subseq[i];
	    if(i != subseq.size() - 1)
	       cout << ' ';
	 }
      cout << endl;
      cin >> n;
   }
   return 0;
}
