#include <iostream>
#include <algorithm>

for(int i = 3; i <= n; ++i)
{
   if(sieve[i]) //if i is prime
   {
      //eliminate multiples of i
      for(int j = 2; i * j <= n; ++j)
	 sieve[i*j] = false;
   }
}

bool NOUN(string s)
{
   return (s=="tom"||s=="jerry"||s =="goofy"||s=="mickey"||s=="jimmy"||s=="dog"
	   ||s=="cat"||s=="mouse") ? true : false;
}

bool ACTOR(string s)
{
   if(NOUN(s)) return true;

   //articles can only be of length 1 or 3 so no sense in trying to split
   //in other positions of the string
   if((ARTICLE(s.substr(0,1)) && NOUN(s.substr(1,s.length()-1)))
      || (ARTICLE(s.substr(0,3)) && NOUN(s.substr(3,s.length()-3))))
      return true;

   return false;
}


bool ACTION(string s)
{
   for(int i = 1; i < s.length()-2; ++i)
   {
      //try splitting string 3 non-empty substrings at all possible positions
      if(ACTIVE_LIST(s.substr(0,i)))
      {
	 for(int j = 1; j < s.length()-i; ++j)
	 {
	    if(VERB(s.substr(i,j)) && ACTIVE_LIST(s.substr(i+j,s.length()-i-j)))
	       return true;
	 }
      }
   }
   return false;
}
