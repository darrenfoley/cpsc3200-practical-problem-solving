/*
  Note: Compile using C++11
  General Approach: Brute force generation of possible answers using a for loop
  from 0000 - 9999 (can use index of for loop to generate each possibility by
  setting the width to 4 and and fill to 0).  Then check to see if each
  possible answer is consistent with the information given in the guesses and
  guess feedback.
*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <tuple>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

bool is_consistent(const vector<tuple<string,int,int> >&guesses, const string &current)
{
   int correctInPlace, correctWrongPlace;
   string guess, curr;
   
   for(int i = 0; i < guesses.size(); ++i)
   {
      correctInPlace = 0;
      correctWrongPlace = 0;
      guess = get<0>(guesses[i]);
      curr = current;
      
      //count how many are in the correct place
      for(int d = 0; d < 4; ++d)
      {
	 if(guess[d] == curr[d])
	 {
	    ++correctInPlace;
	    guess[d] = curr[d] = 'x';
	 }
      }

      //count how many are correct, in the wrong place
      for(int x = 0; x < 4; ++x)
	 for(int y = 0; y < 4; ++y)
	 {
	    //if it has already been used, don't use it again
	    if(curr[x] == 'x')
	       break;
	    //otherwise check if there is a match
	    if(curr[x] == guess[y])
	    {
	       ++correctWrongPlace;
	       curr[x] = guess[y] = 'x';
	       break;
	    }
	 }

      if(get<1>(guesses[i]) != correctInPlace or get<2>(guesses[i]) != correctWrongPlace)
	 return false;
   }

   return true;
}

void solve(const vector<tuple<string,int,int> > &guesses)
{
   bool found = false;
   string answer;
   
   for(int i = 0; i < 10000; ++i)
   {
      ostringstream current;
      current << setfill('0') << setw(4) << i;
      // cout << current.str() << endl;
      if(found)
      {
	 if(is_consistent(guesses, current.str()))
	 {
	    cout << "indeterminate\n";
	    return;
	 }
      }
      else if(found = is_consistent(guesses, current.str()))
      {
	 answer = current.str();
      }
   }
   if(found)
   {
      cout << answer << endl;
      return;
   }
   cout << "impossible\n" ;
}



int main()
{
   int n, g, correctInPlace, correctWrongPlace;
   string guess;
   char garbage;
   
   cin >> n;
   
   for(int caseNo = 0; caseNo < n; ++caseNo)
   {
      vector<tuple<string,int,int> > guesses;
      cin >> g;
      for(int i = 0; i < g ; ++i)
      {
	 cin >> guess >> correctInPlace >> garbage >> correctWrongPlace;
	 guesses.push_back(make_tuple(guess, correctInPlace, correctWrongPlace));
      }
      solve(guesses);
   }
   return 0;
}
