/*
  General Approach: Board is stored in a 2D bool array, where false means "bad"
  square and true means "good" square.  Placement of queens is represented in
  a 1D int array where the indices are the row numbers and columns are the
  values stored at each index.  By the nature of storing things this way,
  we don't need to worry about queen conflicts in the same row/column.  We
  only need to check diagonals.

  We then recursively try different permutations of the above mentioned
  1D array, "pruning" as soon as we see something that won't work so that
  we don't waste time following a path that we already know will not lead
  anywhere.  This is done by checking if swapping the current entry in the
  1D array with each other entry following it in the array leads to a conflict
  in the diagonals/antidiagonals (each stored in an array of size 27 (2*MAX-1))
  as well as checking if we have placed something on a "bad" square.  If any
  of these things are true, we prune and move on to the next potential swap
  (AKA permutation).


*/

#include <iostream>
#include <algorithm>
using namespace std;

int Count;
int CaseNo = 1;

void search(const bool board[14][14], int *colPos, bool *diag, bool *antiDiag, const int n, int index=0)
{
   if(index==n)
   {
      ++Count;
      return;
   }
   for(int i = index; i < n; ++i)
   {
      /*
	Putting the swaps inside the if statement and checking colPos[i] instead of colPos[index] (since
	their values havent been swapped yet) saves approx .4 seconds in the run time.
	Basically we are checking to see if it's even worth swapping before doing it.
      */
      if(board[index][colPos[i]] and !diag[index - colPos[i] + n - 1] and !antiDiag[index + colPos[i]])
      {
	 swap(colPos[index], colPos[i]);
	 diag[index - colPos[index] + n - 1] = true;
	 antiDiag[index + colPos[index]] = true;
	 search(board, colPos, diag, antiDiag, n, index+1);
	 diag[index - colPos[index] + n - 1] = false;
	 antiDiag[index + colPos[index]] = false;
	 swap(colPos[index], colPos[i]);
      }
   }
}

void solve(const bool board[14][14], const int n)
{
   int colPos[14];
   bool diag[27] = {0};
   bool antiDiag[27] = {0};
   for(int i = 0; i < n; ++i)
      colPos[i] = i;
   
   search(board, colPos, diag, antiDiag, n);

   cout << "Case " << CaseNo++ << ": " << Count << endl;
}

int main()
{
   int n;
   char temp;
   cin >> n;
   while(n != 0)
   {
      bool board[14][14] = {0};
      Count = 0;
      for(int i = 0; i < n; ++i)
	 for(int j = 0; j < n; ++j)
	 {
	    cin >> temp;
	    if(temp == '.')
	       board[i][j] = true;
	 }
      solve(board, n);
      cin >> n;
   }
   return 0;
}
