/*
  General Approach: We rephrase the question to be: Given n access points and
  d distance between access points, can we cover all the houses on the
  street?  Thinking of things this way, we can do a binary search,
  where "lo" always represents a value for d that is not possible, and "hi"
  represents a value that is possible.  When "hi" and "lo" are close enough
  (I used < 1e-2), then the value of "hi" will be the minimum value of d, which
  will cover all houses on the street, which is what the problem asks for.
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
using namespace std;

bool covers_all_houses(const vector<int> &houses, int n, double dist)
{
   int cur_house = 0;
   double cur_pos = houses[0];
   for(int i = 1; i <= n; ++i)
   {
      while(houses[cur_house] <= (cur_pos + 2*dist))
      {
	 ++cur_house;
	 if(houses.size() <= cur_house)
	    return true;
      }
      cur_pos = houses[cur_house];
   }
   return false;
}


void solve(const vector<int> &houses, int n)
{
   double hi = houses[houses.size()-1] - houses[0], lo = 0, mid;
   
   while(hi-lo >= 1e-2)
   {
      mid = (hi + lo) / 2;

      if(covers_all_houses(houses, n, mid))
	 hi = mid;
      else
	 lo = mid;
   }
   cout << fixed << setprecision(1) << hi << endl;
}

int main()
{
   int numCases, n, m, temp;

   cin >> numCases;
   for(int i = 0; i < numCases; ++i)
   {
      vector<int> houses;
      cin >> n >> m;
      for(int j = 0; j < m; ++j)
      {
	 cin >> temp;
	 houses.push_back(temp);
      }
      sort(houses.begin(), houses.end());
      solve(houses,n);
   }
   return 0;
}
