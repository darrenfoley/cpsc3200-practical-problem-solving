//compile using c++11
/**
   General approach: grab first word (the country) from each line and throw
   the rest of the words on that line away.  Keep a map with the country as the
   key and the number of conquests as the value.  If a country does not exist
   in the map, add it and set the value to 1.  If it does exist in the map,
   simply increment the value.  Then loop through all the entries in the map
   and, for each, output their key followed by the value.  Since map is sorted,
   the results will be in alphabetical order already.
**/

#include <iostream>
#include <string>
#include <map>

using namespace std;

int main()
{
   int numLines;
   string country;
   string garbage;
   map<string, int> conquests;

   cin >> numLines;

   for (int i = 0; i < numLines; i++)
   {
      cin >> country;
      getline(cin, garbage);

      if ((conquests.find(country)) == conquests.end())
	 conquests[country] = 1;
      else
	 conquests[country] += 1;
   }

   for (auto i : conquests)
      cout << i.first << ' ' << i.second << endl;

   return 0;
}
