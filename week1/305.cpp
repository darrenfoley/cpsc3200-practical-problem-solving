//compile using c++11
/**
   General approach: is to try values of m starting at k (since values less than
   k will immediately kill a good guy) using brute force (increase m by 1 each
   time it fails until a suitable value of m is found).
   Since the k good guys are first (starting at 0), we can simply test to see if
   the current person being killed (stored as an int) is less than k.  If it is,
   the person being killed is a good guy.  If it is greater than or equal to k,
   then a bad guy is being killed.
   If the number of remaining people == k, then we know only good guys remain,
   and the current value of m will work.

   In order to speed up duplicate queries (with same value for k), we remember
   solutions as they are calculated and check if the solution exists already
   before attempting to calculate it again
**/

#include <iostream>
#include <unordered_map>

using namespace std ;

//given k, returns a minimum m which will kill all the bad guys first, before killing a good guy
int findNum(int k)
{
   int current ; //"index" of person we are currently "killing"
   int m = k ;  //num of people to traverse before "killing"
   int numPeople ; //number of people still alive

   while(true)
   {
      numPeople = 2 * k ;
      current = (m - 1) % numPeople ;

      while(true)
      {
	 if(current < k) //if a good guy is being killed
	 {
	    if (numPeople == k) //if only good guys remain
	       return m ;  //good m, return
	    break ;  //bad m, try another
	 }
	 numPeople--; //bad guy "killed"
	 current = (current + m - 1) % numPeople ; //find new current
      }
      m++ ; //update m to next possibility
   }
}

int main()
{
   int k;
   unordered_map<int,int> memo;  //use to store solutions we already know so as to save time by not calculating the same answer twice

   cin >> k ;
   while (k != 0)
   {
      if(!memo[k])
	 memo[k] = findNum(k) ;
      cout << memo[k] << endl ;
      cin >> k ;
   }

   return 0 ;
}
