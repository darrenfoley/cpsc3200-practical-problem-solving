//compile using c++11
/**
   General Approach: Read all keywords into one vector and all excuses into
   another.  For each excuse, replace all punctuation with spaces and all
   uppercase letters with lowercase letters.  Then parse one word at a time
   (where word is defined as a string delimited by a space or newline character
   (after punctuation has been converted into spaces)), checking to see if
   the word matches any of the keywords.  If a match is found, we increment
   the number of keyword occurances found and break from the loop.  Once all
   words in the excuse have been parsed, the score is returned and checked
   against the current max score.  The excuse(s) with the max score is stored
   in a vector or worst excuses, which is cleared when an excuse with a worse
   score is found, until all excuses have been scored.  Then the worse excuses
   are outputted.
 **/

#include <iostream>
#include <string>
#include <sstream>
#include <cctype>
#include <vector>

using namespace std;

//given an excuse and a vector of keywords, returns a score representing how
//many occurances of keywords are found in the excuse
int numKeywords(string excuse, const vector<string> & keywords)
{
   
   //replace all punctuation in excuse with spaces and all uppercase letters
   //with lowercase letters
   for (unsigned int i = 0; i < excuse.length(); i++)
   {
      if (ispunct(excuse[i]))
	 excuse[i] = ' ' ;
      else if (isupper(excuse[i]))
	 excuse[i] = tolower(excuse[i]) ;
   }

   istringstream iss(excuse) ;
   string token ;
   int count = 0 ;
   
   //parse delimited (by space or punctuation) tokens individually,
   //comparing them to the list of keywords and counting how many matches
   //we find
   while (iss >> token)
   {
      for (auto i : keywords)
	 if (i == token)
	 {
	    count++ ;
	    break ;
	 }
   }

   return count;
}


int main()
{
   int K, E, setNo = 1;
   
   while (cin >> K >> E)
   {
      vector <string> keywords, excuses;
      string str;

      for (int i = 0; i < K; i++)
      {
	 cin >> str;
	 keywords.push_back(str);
      }

      cin.ignore();

      for (int i = 0; i < E; i++)
      {
	 getline(cin, str);
	 excuses.push_back(str);
      }

      //setup for variables needed when finding worst excuse(s) (excuse(s)
      //with most (max) number of keyword occurances
      int max = 0; // to store max number of keyword occurances found in an excuse so far
      int curCount; // num of keyword occurances in current excuse
      vector<string> worstExcuses; //container to store worst excuses in for output

      for (unsigned int i = 0; i < excuses.size(); i++)
      {
	 curCount = numKeywords(excuses[i], keywords);
	 if (curCount > max)
	 {
	    max = curCount;
	    worstExcuses.clear();
	    worstExcuses.push_back(excuses[i]);
	 }
	 else if (curCount == max)
	    worstExcuses.push_back(excuses[i]);
      }

      //output
      cout << "Excuse Set #" << setNo << endl;
      for (auto i : worstExcuses)
	 cout << i << endl;
		
      cout << endl;

      setNo++;
   }
   return 0;
}
