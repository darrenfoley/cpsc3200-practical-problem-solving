//compile using c++11
/**
   General Approach: For each line of input, read 2 integers, subtract the
   second from the first, and output the absolute value of this difference.
**/

#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
   //int is not big enough to hold input as it can be as big as 2^32 and int
   //can only store positive values up to (2^31)-1, so I use long long
   long long a, b;

   while(cin >> a)
   {
      cin >> b;
      //do not need to worry about figuring out which is army is bigger/smaller,
      //just find the difference and make it positive
      cout << abs(a-b) << endl;
   }
   return 0;
}
