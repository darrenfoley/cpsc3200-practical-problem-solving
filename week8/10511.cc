/*
  Note: compile using C++11

  General Approach:
  We solve this as a network flow problem in which the graph is set up
  as follows:

  -There are edges from source to each of the clubs with weight 1
  -There are edges from each of the clubs to each of the people belonging
  to that club with weight 1
  -There are edges from each of the people to the party to which they belong
  with weight 1
  -There are edges from each of the parties to the sink, with weight
  (number of clubs -1) / 2

  Then, if the max network flow is less than the number of clubs, we can
  say it's impossible.

  Otherwise we need to look at each of the edges between each club
  and the people which belong to it and, if the weight flow is greater
  than 0 we know that that person is representing that club.
*/

/*
 * Network Flow
 *
 * Author: Howard Cheng
 *
 * The routine network_flow() finds the maximum flow that can be
 * pushed from the source (s) to the sink (t) in a flow network
 * (i.e. directed graph with capacities on the edges).  The maximum
 * flow is returned.  Note that the graph is modified.  If you wish to
 * recover the flow on an edge, it is in the "flow" field, as long as
 * is_real is set to true.
 *
 * Note: if you have an undirected network. simply call add_edge twice
 * with an edge in both directions (same capacity).  Note that 4 edges
 * will be added (2 real edges and 2 residual edges).  To discover the
 * actual flow between two vertices u and v, add up the flow of all
 * real edges from u to v and subtract all the flow of real edges from
 * v to u.  (In fact, for a residual edge the flow is always 0 in this
 * implementation.)
 *
 * This code can also be used for bipartite matching by setting up an
 * appropriate flow network.
 *
 * The code here assumes an adjacency list representation since most
 * problems requiring network flow have sparse graphs.
 *
 * This is the basic augmenting path algorithm and it is not the most
 * efficient.  But it should be good enough for most programming contest
 * problems.  The complexity is O(f m) where f is the size of the flow
 * and m is the number of edges.  This is good if you know that f
 * is small, but can be exponential if f is large.
 *
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <cassert>
#include <map>
#include <sstream>

using namespace std;

struct Edge;
typedef list<Edge>::iterator EdgeIter;

struct Edge {
   int to;
   int cap;
   int flow;
   bool is_real;
   EdgeIter partner;
  
   Edge(int t, int c, bool real = true)
      : to(t), cap(c), flow(0), is_real(real)
   {};

   int residual() const
   {
      return cap - flow;
   }
};

struct Graph {
   list<Edge> *nbr;
   int num_nodes;
   Graph(int n)
      : num_nodes(n)
   {
      nbr = new list<Edge>[num_nodes];
   }

   ~Graph()
   {
      delete[] nbr;
   }

   // note: this routine adds an edge to the graph with the specified capacity,
   // as well as a residual edge.  There is no check on duplicate edge, so it
   // is possible to add multiple edges (and residual edges) between two
   // vertices
   void add_edge(int u, int v, int cap)
   {
      nbr[u].push_front(Edge(v, cap));
      nbr[v].push_front(Edge(u, 0, false));
      nbr[v].begin()->partner = nbr[u].begin();
      nbr[u].begin()->partner = nbr[v].begin();
   }
};

void push_path(Graph &G, int s, int t, const vector<EdgeIter> &path, int flow)
{
   for (int i = 0; s != t; i++) {
      if (path[i]->is_real) {
	 path[i]->flow += flow;
	 path[i]->partner->cap += flow;
      } else {
	 path[i]->cap -= flow;
	 path[i]->partner->flow -= flow;
      }
      s = path[i]->to;
   }
}

// the path is stored in a peculiar way for efficiency: path[i] is the
// i-th edge taken in the path.
int augmenting_path(const Graph &G, int s, int t, vector<EdgeIter> &path,
		    vector<bool> &visited, int step = 0)
{
   if (s == t) {
      return -1;
   }
   for (EdgeIter it = G.nbr[s].begin(); it != G.nbr[s].end(); ++it) {
      int v = it->to;
      if (it->residual() > 0 && !visited[v]) {
	 path[step] = it;
	 visited[v] = true;
	 int flow = augmenting_path(G, v, t, path, visited, step+1);
	 if (flow == -1) {
	    return it->residual();
	 } else if (flow > 0) {
	    return min(flow, it->residual());
	 }
      }
   }
   return 0;
}

// note that the graph is modified
int network_flow(Graph &G, int s, int t)
{
   vector<bool> visited(G.num_nodes);
   vector<EdgeIter> path(G.num_nodes);
   int flow = 0, f;

   do {
      fill(visited.begin(), visited.end(), false);
      if ((f = augmenting_path(G, s, t, path, visited)) > 0) {
	 push_path(G, s, t, path, f);
	 flow += f;
      }
   } while (f > 0);
  
   return flow;
}

int main()
{
   int T;
   string line, club, name, party;
   
   
   cin >> T; cin.ignore();
   //get rid of blank line
   getline(cin, line);
   
   for(int caseNo = 0; caseNo < T; ++caseNo)
   {
      //should be around 40000 nodes max -- I used 41000 to be safe
      //I'll use node 0 as the src and node 40999 as the sink
      Graph g(41000);
      map<string, int> people, clubs, parties;
      string numToName[41000];
      int curNode = 1;
      getline(cin, line);
      
      while(line.length() != 0)
      {
	 istringstream iss(line);
	 iss >> name >> party;
	 people[name] = curNode;
	 numToName[curNode++] = name;
	 if(parties.find(party) == parties.end())
	    parties[party] = curNode++;
	 g.add_edge(people[name], parties[party], 1);
	 
	 while(iss >> club)
	 {
	    if(clubs.find(club) == clubs.end())
	       clubs[club] = curNode++;
	    g.add_edge(clubs[club], people[name], 1);
	 }
	 getline(cin, line);
      }

      //connect the src to the clubs
      for(auto it = clubs.begin(); it != clubs.end(); ++it)
	 g.add_edge(0, it->second, 1);

      //connect the parties to the sink
      int numClubs = clubs.size();
      int councilMembersPerParty = (numClubs - 1)/2;

      for(auto it = parties.begin(); it != parties.end(); ++it)
	 g.add_edge(it->second, 40999, councilMembersPerParty);

      //run the network flow algorithm
      if(network_flow(g, 0, 40999) < numClubs)
      {
	 cout << "Impossible.\n";
	 if(caseNo != T - 1)
	    cout << endl;
	 continue;
      }

      //find which people represent which clubs and print results
      vector<string> results;
      for(auto club : clubs)
      {
	 for(auto edge : g.nbr[club.second])
	    if(edge.is_real && edge.flow > 0)
	    {
	       cout << numToName[edge.to] << ' ' << club.first << endl;;
	       break;
	    }
      }
      
      if(caseNo != T - 1)
	 cout << endl;
   }

   return 0;
}


