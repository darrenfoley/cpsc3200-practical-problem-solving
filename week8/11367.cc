/*
  General Approach:
  For this problem we need to think of the graph in a different way,
  with the nodes being states consisting of city and fuel level.
  We then have 0 weighted edges from (city1,fuel1) to
  (city2,fuel1 - dist(city1, city2)).
  We also have price weighted edges from (city1,fuel1) to (city1,fuel1 + 1).

  It's a bit messy to build this graph explicitly, so I instead modify
  Howard's Dijkstra code to look at neighbours which we generate on the fly...
  So it is like an implied graph which references the graph with the cities
  as nodes and distances between cities as edge weights in order to see if
  we have enough gas to get from one city to another.

  At each step we try staying at the current city and fueling up (if we're not
  at capacity already), and also try going to each neighbouring city, if we have
  enough gas.

  Although I stated above that the states consist of (city,fuel level) I
  actually store my states as (money spent, city, fuel level) just because this
  turns out to be a little more convenient.
*/

/*
 * Dijkstra's Algorithm for sparse graphs
 *
 * Author: Howard Cheng
 *
 * Given a weight matrix representing a graph and a source vertex, this
 * algorithm computes the shortest distance, as well as path, to each
 * of the other vertices.  The paths are represented by an inverted list,
 * such that if v preceeds immediately before w in a path from the
 * source to vertex w, then the path P[w] is v.  The distances from
 * the source to v is given in D[v] (-1 if not connected).
 *
 * Call get_path to recover the path.
 *
 * Note: Dijkstra's algorithm only works if all weight edges are
 *       non-negative.
 *
 * This version works well if the graph is not dense.  The complexity
 * is O((n + m) log (n + m)) where n is the number of vertices and
 * m is the number of edges.
 *
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <cassert>
#include <queue>

using namespace std;

int prices[1000];
int c, e;

struct State
{
   State(int s, int c, int f) : spent(s), city(c), fuel(f) {}
   int spent;
   int city;
   int fuel;
};

class Compare
{
public:
   bool operator() (const State &a, const State &b) { return a.spent > b.spent ? true : false; }
};

struct Edge {
   int to;
   int weight;       // can be double or other numeric type
   Edge(int t, int w)
      : to(t), weight(w) { }
};
  
typedef vector<Edge>::iterator EdgeIter;

struct Graph {
   vector<Edge> *nbr;
   int num_nodes;
   Graph(int n)
      : num_nodes(n)
   {
      nbr = new vector<Edge>[num_nodes];
   }

   ~Graph()
   {
      delete[] nbr;
   }

   // note: There is no check on duplicate edge, so it is possible to
   // add multiple edges between two vertices
   //
   // If this is an undirected graph, be sure to add an edge both
   // ways
   void add_edge(int u, int v, int weight)
   {
      nbr[u].push_back(Edge(v, weight));
   }
};

/* assume that D and P have been allocated */
// D now indexed by D[city][fuel]
// used now indexed by used[city][fuel]
void dijkstra(const Graph &G, int src)
{
   int n = G.num_nodes;
   vector<vector<bool> > used(n, vector<bool>(c+1, false));
   vector<vector<int> > D(n, vector<int>(c+1, -1));
   priority_queue<State, vector<State>, Compare> fringe;
   
   D[src][0] = 0;
   fringe.push(State(0,src,0));

   while (!fringe.empty())
   {
      State next = fringe.top();
      fringe.pop();

      //if we've made it to the end city we can print how much we've spent and return
      if(next.city == e)
      {
	 cout << next.spent << endl;
	 return;
      }
      
      //if we've been here before, skip it
      if (used[next.city][next.fuel])
	 continue;
      //otherwise mark it as visited
      used[next.city][next.fuel] = true;

      //we can either stay and fuel up or move to a neighbouring city, so we try both

      //try adding more fuel if we're not full and we haven't been in that state before
      if(next.fuel != c and !used[next.city][next.fuel+1])
      {
	 D[next.city][next.fuel+1] = next.spent + prices[next.city];
	 fringe.push(State(next.spent+prices[next.city], next.city, next.fuel + 1));
      }

      //try going to each of the neighbouring cities
      for (EdgeIter it = G.nbr[next.city].begin(); it != G.nbr[next.city].end(); ++it)
      {
	 int v = it->to;

	 //if we don't have enough gas or we've already been in the neighbour with that fuel
	 //level, skip it
	 if (next.fuel - it->weight < 0 or used[v][next.fuel - it->weight])
	    continue;
	 
	 //otherwise we check if we haven't been to the neighbour with this amount of gas or if
	 //we have found a cheaper path to the neighbour and if so update it and add it
	 if (D[v][next.fuel - it->weight] == -1 or next.spent < D[v][next.fuel - it->weight])
	 {
	    D[v][next.fuel - it->weight] = next.spent;	    
	    fringe.push(State(next.spent, v, next.fuel - it->weight));
	 }
      }
   }
   //if the queue is empty and we haven't returned yet, then it must be impossible
   cout << "impossible\n";
}

int main()
{
   int n, m, q, u, v, d, s;
   cin >> n >> m;
   Graph g(n);
   
   for(int i = 0; i < n; ++i)
      cin >> prices[i];

   for(int i = 0; i < m; ++i)
   {
      cin >> u >> v >> d;
      g.add_edge(u, v, d);
      g.add_edge(v, u, d);
   }
   
   cin >> q;
   for(int i = 0; i < q; ++i)
   {
      cin >> c >> s >> e;
      dijkstra(g, s);
   }
   return 0;
}
