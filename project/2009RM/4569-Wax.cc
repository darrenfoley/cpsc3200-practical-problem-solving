/*
  General Approach:
  -for each slice we figure out the area we want (rather than starting from
  the end point of the previous slice we just start at the bottom right corner
  and give the nth person n*areaPerPerson
  -find the corner that gives us more area than we want if we choose it as the
  3rd point of the triangle
  -we can find length of triangle base on the wall before the corner we found
  since we know the height (it can be derived based on which wall the point will
  lay on) and the area
  -once we have length of triangle base we can use this information along with the
  coordinates of the other point of the triangle which lays on that wall to get
  the final point of the triangle
  
  My solution doesn't get accepted due to rounding errors causing it to be off by
  +-0.001 in certain cases -- Howard said this was sufficient
*/


#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>
using namespace std;

//n = workers
int width, height, door, workers;

struct Point
{
   Point() {}
   Point(double _x, double _y) : x(_x), y(_y) {}
   double x;
   double y;
};

double myRound(double x)
{
   return floor(x * 1000 + 0.5) / 1000;
}

Point get_point(double area, Point last, int wall)
{
   int h;
   //right wall
   if(wall == 0)
      h = width - door;
   //Back wall
   else if(wall == 1)
      h = height;
   //left wall
   else
      h = door;

   double b = (2*area)/h;

   Point p;
   if(wall == 0)
      p = Point(static_cast<double> (width), last.y + b);
   else if (wall == 1)
      p = Point(last.x - b, static_cast<double>(height));
   else
      p = Point(0.0, last.y - b);

   if(p.x < 0.0)
      p.x = 0.0;
   if(p.y < 0.0)
      p.y = 0.0;
   return p;
}

void solve()
{
   Point d(door, 0);
   
   for(int i = 1; i <= workers-1; ++i)
   {
      Point last(width,0);
      double curArea = static_cast<double>(width*height*i)/workers;

      //on right wall
      double tempArea = 0.5 * (height - last.y) * (width - d.x);
      
      //if choosing the corner is more than the area we want
      if(tempArea > curArea)
      {
	 last = get_point(curArea, last, 0);
	 last.x = myRound(last.x); last.y = myRound(last.y);
	 cout << fabs(last.x) << ' ' << fabs(last.y);
	 if(i != workers - 1)
	    cout << ' ';
	 continue;
      }
      else
      {
	 last = Point(width,height);
	 curArea -= tempArea;
      }

      //back wall
      tempArea = 0.5 * last.x * height;
	 
      if(tempArea > curArea)
      {
	 last = get_point(curArea, last, 1);
	 last.x = myRound(last.x); last.y = myRound(last.y);
	 cout << fabs(last.x) << ' ' << fabs(last.y);
	 if(i != workers-1)
	    cout << ' ';
	 continue;
      }
      else
      {
	 last = Point(0,height);
	 curArea -= tempArea;
      }	 

      //left wall
      last = get_point(curArea, last, 2);
      last.x = myRound(last.x); last.y = myRound(last.y);
      cout << fabs(last.x) << ' ' << fabs(last.y);
      if(i != workers-1)
	 cout << ' ';
   }
   cout << endl;
}


int main()
{
   cin >> width >> height >> door >> workers;
   cout << setprecision(3) << fixed;

   while(width !=0 || height != 0 || door != 0 || workers !=0)
   {
      solve();
      cin >> width >> height >> door >> workers;
   }
 
   return 0;
}
