/*
	General Approach: Solving this problem was more or less just a matter of parsing a bunch of information
	in the question and figuring out what needs to be stored (and how) as well as when and how to update
	the stored information.
	I chose to store the information in the 2D array named instrumentInfo.
	instrumentInfo[i][0] stores the fixed fee for the ith instrument.
	instrumentInfo[i][1] stores the percentage fee for the ith instrument.
	instrumentInfo[i][2] stores the principal for the ith instrument.
	instrumentInfo[i][3] stores the percentage (as a decimal) of the total
	initial investment in all instruments that the ith instrument makes up.
	This percentage is used when rebalancing.
	There are comments throughout the code indicating where each calculation (which is fairly straightforward)
	takes place.
	*/

#include <iostream>
#include <iomanip>

using namespace std;

void get_investment_info(const int nInstruments, double instrumentInfo[10][4])
{
	double total = 0;
	
	/* input */
	//get fixed fee
	for(int i = 0; i < nInstruments; ++i)
		cin >> instrumentInfo[i][0];
	//get percentage fee
	for(int i = 0; i < nInstruments; ++i)
		cin >> instrumentInfo[i][1];
	//get principals
	for(int i = 0; i < nInstruments; ++i)
	{
		cin >> instrumentInfo[i][2];
		total += instrumentInfo[i][2];
	}
	//get original ratios
	for(int i = 0; i < nInstruments; ++i)
		instrumentInfo[i][3] = instrumentInfo[i][2] / total;
}

void rebalance(double instrumentInfo[10][4], const int nInstruments)
{
	double total = 0;
	//add up all the investments
	for(int j = 0; j < nInstruments; ++j)
		total += instrumentInfo[j][2];
	//redistribute them based on the ratios
	for(int j = 0; j < nInstruments; ++j)							
		instrumentInfo[j][2] = total * instrumentInfo[j][3];
}

void term_calculations(double instrumentInfo[10][4], const int nInstruments, const int nRebalance, const int nTerms, const int term)
{
	double ret;
	for(int j = 0; j < nInstruments; ++j)
	{
		cin >> ret;
		/*
			if the principal is greater than 0, we update it based on the return, the fixed fee,
			and the percentage fee.
		*/
		if( instrumentInfo[j][2] > 0)
		{
			//calculate the return
			ret *= instrumentInfo[j][2]; 
			//subtract the fixed and percentage fees from the principal
			instrumentInfo[j][2] -= (instrumentInfo[j][1] * instrumentInfo[j][2] + instrumentInfo[j][0]);
			//add the return to the principal
			instrumentInfo[j][2] += ret;
		}
		/*
			otherwise if the investment is <= 0, we just set it to 0
		*/
		else
			instrumentInfo[j][2] = 0;
	}
	//here we deal with rebalancing
	if(term % nRebalance == 0 and term != nTerms)
		rebalance(instrumentInfo, nInstruments);
}

int main()
{
	int nInstruments, nTerms, nRebalance;
	bool first = true;
	while(cin >> nInstruments >> nTerms >> nRebalance)
	{
		double instrumentInfo[10][4];
		get_investment_info(nInstruments, instrumentInfo);
		
		/* term calculations */
		for(int i = 1; i <= nTerms; ++i)
			term_calculations(instrumentInfo, nInstruments, nRebalance, nTerms, i);
		
		//here we deal with the output
		cout << setprecision(2) << fixed;
		for (int i = 0; i < nInstruments; ++i)
		{
			cout << instrumentInfo[i][2] ;
			if(i != nInstruments - 1)
				cout << ' ';
		}
		cout << endl;
	}
	return 0;
}