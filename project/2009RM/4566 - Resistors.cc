/*
	General Approach: I created a Resistor class for convenience (as well as to refresh my OOP skills a little)
	and then used it while evaluating each (infix) expression using 2 stacks.  The trick here is to recognize
	that the problem is basically just defining some new operators (& and |) and asking you to evaluate it as
	an infix expression.  I looked at Howard's infix code a little bit, but decided to use my own rather than
	adapt it to use Resistors.  Most of it I came up with without looking.  I decided it was overkill for this
	problem to implement a tokenizer given that there weren't that many operators and all the expressions
	were gauranteed to be valid.
*/

#include <iostream>
#include <algorithm>
#include <sstream>
#include <stack>

using namespace std;

/* 
	This class just makes things easier for me.  It's very similar to a rational number class, with 
	some additional operators and functions which are specific to this problem.  It allows me to do things like
	Resistor(3/4) & Resistor(7/8), which is convenient.
*/
class Resistor
{
	public:
		Resistor() {}
		Resistor(int n, int d) : numer(n), denom(d) { reduce(); }
		Resistor operator|(const Resistor &rhs) const { return (this->recip() + rhs.recip()).recip(); }
		Resistor operator&(const Resistor &rhs) const { return *this + rhs; }
		void print(ostream& os) const { os << numer << '/' << denom; }
	private:
		int numer, denom;
		Resistor recip() const { return Resistor(denom, numer); }
		void reduce() { int temp = gcd(numer, denom); numer /= temp; denom /= temp; }
		int gcd(int a, int b) const { if (b > a) swap(a,b); return b==0 ? a : gcd(b, a%b); }
		Resistor operator+(const Resistor &rhs) const
		{
			int n = numer * rhs.denom + rhs.numer * denom;
			int d = denom * rhs.denom;
			return Resistor(n,d);
		}
};

ostream& operator<<(ostream& os, const Resistor &res)
{
	res.print(os);
	return os;
}

istream& operator>>(istream& is, Resistor &res)
{
	int n, d;
	is >> n;
	is.ignore();
	is >> d;
	res = Resistor(n,d);
	return is;
}

//this function evaluates each circuit as an infix expression with help from the Resistor class
//I do this by keeping 2 stacks, one for operators and one for operands
//at the end of the function, the solution to the expression is on the top of the stack
Resistor evaluate(istringstream &expr)
{
	stack <char> operators;
	stack <Resistor> operands;
	Resistor res, l, r;
	char c;
	/*
		the loop condition I am using here checks the number of characters
		remaining in the istringstream.  I found this on stack overflow.
		I could also just convert it to a string and check the length, but I wanted
		to try this out.
	*/
	while(expr.rdbuf()->in_avail())
	{
		c = expr.peek();
		//if it's a space
		if(c == ' ')
			expr.ignore();
			
		//else if it's a resistor
		else if(isdigit(c))
		{
			expr >> res;
			//if there is an operator on the stack, use it
			if(!operators.empty())
			{
				c = operators.top();
				switch(c)
				{
					case '|':
						l = operands.top(); operands.pop();
						operands.push(l | res);
						operators.pop();
						break;
					case '&':
						l = operands.top(); operands.pop();
						operands.push(l & res);
						operators.pop();
						break;
					default:
						operands.push(res);
				}
			}
			//otherwise just push the operand
			else
				operands.push(res);
		}
		//otherwise it must be one of "()|&" (parenthesis or an operator)
		else
		{
			expr >> c;
			switch(c)
			{
				/*
					if it's a right bracket we need to pop the left bracket and
					then check if there is an operator that needs to be used
				*/
				case ')':
					operators.pop();
					if(!operators.empty())
					{
						c = operators.top();
						if(c == '&')
						{
							r = operands.top(); operands.pop();
							l = operands.top(); operands.pop();
							operands.push(l & r);
							operators.pop();
						}
						else if(c == '|')
						{
							r = operands.top(); operands.pop();
							l = operands.top(); operands.pop();
							operands.push(l | r);
							operators.pop();
						}
					}
					break;
				//operators and left brackets just get pushed onto the stack
				case '(':
				case '|':
				case '&':
					operators.push(c);
					break;
				default:
					continue;
			}
		}
	}
	return operands.top();
}

int main()
{
	string str;
	while(getline(cin, str))
	{
		istringstream expr(str);
		cout << evaluate(expr) << endl;
	}
	return 0;
}
