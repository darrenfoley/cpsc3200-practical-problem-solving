/*
  General Approach: Dynamic programming with the following recurrence:

  Base Cases:
  solvable(x,x) = false
  solvable(x,y), where x > y = true

  xy case:
  solveable(x,y) = true, where solvable(x,i)==true and solvable(i+1,y)==true
  for some i, x+1 <= i < y-1
  
  AxA and AxAxA cases:
  solvable(x,y), where str[x] == str[y] = solvable(x+1,y-1) ||
  (solvable(x+1,i-1) && solvable(i+1,y-1)), for some i, x+1 <= i < y where
  str[i] == str[x]
 */

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

int DP[150][150];
string str;

bool solvable(int s, int e)
{
   //str length = 1 or know it's not solvable already
   if(s == e or DP[s][e] == 0)
      return false;

   //str is empty string or known to be true previously
   if(s > e or DP[s][e] == 1)
      return true;

   //xy case
   for(int i = s + 1; i < e - 1; ++i)
      if(solvable(s,i) and solvable(i + 1, e))
	 return DP[s][e] = true;

   //in both other cases, the first and last letters are the same
   if(str[s] == str[e])
   {
      //AxA case -- x might be empty
      if(solvable(s + 1, e - 1))
	 return DP[s][e] = true;
	 
      //AxAxA case -- x might be empty
      for(int i = s + 1; i < e; ++i)
	 if(str[i] == str[s])
	    if(solvable(s + 1, i - 1) and solvable(i + 1, e - 1))
	       return DP[s][e] = true;
   }

   //if we reach this point then str must not be solveable
   return DP[s][e] = false;
}

int main()
{
   bool ans;
   while(cin >> str)
   {
      fill(DP[0], DP[0] + sizeof(DP)/sizeof(int), -1);
      if(solvable(0, str.length() - 1))
	 cout << "solvable\n";
      else
	 cout << "unsolvable\n";
   }
}
