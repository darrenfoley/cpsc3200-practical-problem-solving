/*
  Note: Compile using c++11
  
  General Approach: First check if desired shuffling is sorted, if it is
  we can just return 0 since we start with sorted cards.

  Next find the cycle of positions each card goes through before returning
  to its initial program.

  After generating each cycle we check if the card is ever in the right
  position.  If not, we know it's impossible so just return -1.
  
  Then we recognize that we have a linear equation for each card in the form 

  a + bx = c

  Where a is the first number of shuffles after which it is in the right
  position, b is the cycle length, x is an integer variable >= 0 and c is a
  number of shuffles which places the card in the correct position.

  Combining any two of these equations gives us a Linear Diophantine Equation

  ax + by = c

  Each card's equation can also be thought of as

  c ~ a mod b (~ meaning congruent)
  
  We can then use a special version of the Chinese Remainder theorem to solve
  these equations for a common c (if there is one).  It is a special version
  in that it allows the mod bases (in this case the cycle lengths) to have
  common factors (each pair of cycle lengths may not be coprime).
 */

#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <math.h>
using namespace std;

typedef long long ll;

vector<ll> cycles[520];
ll shuffleInRightPosition[520];
ll cycleLengths[520];
/*
  Below taken from Howard's World Finals contest algorithms
  Modified to use lls
*/

ll gcd(ll a, ll b, ll &s, ll &t) { // a*s+b*t = g
  if (b==0) { t = 0; s = (a < 0) ? -1 : 1; return (a < 0) ? -a : a;
  } else { ll g = gcd(b, a%b, t, s);  t -= a/b*s;  return g; }
}

ll cra(int n, ll m[], ll a[]) {
  ll u = a[0], v = m[0], p, q, r, t;
  for (int i = 1; i < n; i++) {
    r = gcd(v, m[i], p, q);  t = v;
    if ((a[i] - u) % r != 0) { return -1;  }   // no solution!
    v = v/r * m[i];          u = ((a[i]-u)/r * p * t + u) % v;
    if (u < 0) u += v;  //this line moved in for loop as compared to original code
  }
  return u;
}

/*
  End of code taken from Howard's World Finals contest algorithms
*/
   
bool findCycles(ll N, ll shuffle[], ll want[])
{
   for(ll i = 0; i < N; ++i)
   {
      cycles[i].clear();
      
      ll j=i;
      do
      {
	 cycles[i].push_back(j);
	 j = shuffle[cycles[i].back()];
      }while(cycles[i][0] != j);

      //if the current element is never in the right position, then it's impossible
      auto it = find(cycles[i].begin(), cycles[i].end(), find(want, want + N, i) - want);
      if(it == cycles[i].end())
	 return false;
      //remember the first time when each card was in the right position
      shuffleInRightPosition[i] = it - cycles[i].begin();
   }
   return true;
}

ll solve(ll N, ll shuffle[], ll want[])
{
   if(is_sorted(want, want + N))
      return 0;

   //step 1
   //find the cycles
   if(!findCycles(N, shuffle, want)) return -1;
   
   //step 2 try to find the first shuffle where all cards are in the correct
   //position
   for(ll i = 0; i < N; ++i)
      cycleLengths[i] = cycles[i].size();
   return cra(N,  cycleLengths, shuffleInRightPosition);
}

int main()
{
   ll N, temp;
   ll shuffle[520], want[520];
   
   cin >> N;
   
   while(N != 0)
   {
      for(ll i = 0; i < N; ++i)
      {
	 cin >> temp;
	 shuffle[temp-1] = i;
      }

      for(ll i = 0; i < N; ++i)
      {
	 cin >> temp;
	 want[i] = temp - 1;
      }
      cout << solve(N, shuffle, want) << endl;
      cin >> N;
   }
   return 0;
}
