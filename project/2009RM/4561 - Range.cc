/*
	General approach: Keep track of distance travelled and fuel consumed, only updating them
	when the fuel decreases.  We always keep track of the previous odometer and previous fuel
	amounts so that when we get the new values from input we can calculate the difference.
*/

#include <iostream>
#include <math.h>

using namespace std;

double solve_rec(double prevOD, double prevFuel, double fuelConsumed = 0, double distanceTravelled = 0)
{
	double currOD, currFuel;
	
	cin >> currOD >> currFuel;
	
	if(currOD == 0 and currFuel == 0)
		return (distanceTravelled / fuelConsumed) * prevFuel;
	
	if((prevFuel - currFuel) > 0)
	{
		fuelConsumed = fuelConsumed + (prevFuel - currFuel);
		distanceTravelled = distanceTravelled + (currOD - prevOD);
	}
		
	return solve_rec(currOD, currFuel, fuelConsumed, distanceTravelled);
}

int main()
{
	double startOD, startFuel;
	
	cin >> startOD >> startFuel;
	
	while(startOD != -1 or startFuel != -1)
	{
		cout << round(solve_rec(startOD, startFuel)) << endl;
		cin >> startOD >> startFuel;
	}
	
	return 0;
}