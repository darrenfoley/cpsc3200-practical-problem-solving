/*
	Note: I found it easier to think of Row # 1 being the top row, while the problem represented the bottom Row as
	Row # 1.  This is why whenever I am outputting a position, I ouput -(row-11) to flip the value into the correct
	format.
	
	General Approach: Since this is essentially an ad-hoc problem , I had to combine a number of strategies to
	solve it. I'll list the strategies I used and where I used them below.
	
	-depth-first search(used in function find_cluster_size): used to recursively find the size of the cluster which
	the ball in the current position belongs to.
	
	-delta arrays (used in function find_cluster_size): used in combination with a for loop to iterate through the 4
	recursive calls.
	
	-board padding (throughout the problem): I padded the board with blank characters ('#') to avoid having to type a bunch
	of if statements
	
	I also used a trick for compressing the board, where I was able to compress both downward and to the left at the
	same time.  I did this by storing each column as a string in a vector.  For each column, I iterate through it
	upwards, adding only 'R's, 'G's, and 'B's to it (this compresses vertically).  When I am finished iterating through
	the column, I only push the string onto the vector if its size is greater than 0 (this compresses to the left,
	since only columns with no balls in them will result in strings of size 0).
	
	The rest was more a matter of making sure to implement all the rules of the game correctly and display the output
	correctly than it was applying any one particular strategy.
*/

#include <iostream>
#include <math.h>
#include <vector>
using namespace std;

struct Pos
{
	Pos(int r, int c): row(r), col(c){}
	Pos() {}
	int row, col;
};

int dr[] = {-1, 0, 1, 0};
int dc[] = {0, -1, 0, 1};

char board[12][17] ;

int score, moveNum, ballsRemaining;

//use depth-first search to find the size of the cluster to which the current ball belongs
//0 is returned if we have already visited the ball, it is the wrong color, or if it is not a valid
//position
int find_cluster_size(bool visited[12][17], const Pos &current, const char curColour, Pos &anchor)
{
	//if it's not a valid position, it's been visited, or the current position is a different
	//colour than that which we were previously looking at, we just return 0
	if(visited[current.row][current.col] || (board[current.row][current.col] != curColour) )
		return 0;
	
	int result = 1;
	
	//otherwise we need to do some book keeping, check the anchor, and recurse in 4 directions
	visited[current.row][current.col] = true;
	if(current.col < anchor.col)
		anchor = current;
	else if(current.col == anchor.col)
		if(current.row > anchor.row)
			anchor = current;
	for(int i = 0; i < 4; ++i)
		result += find_cluster_size(visited, Pos(current.row + dr[i], current.col + dc[i]), curColour, anchor);
		
	return result;
}

/*
	returns the position of the "anchor" of the largest cluster for each turn of the game.
	If there is no cluster of size > 1, it returns Pos(0,0)
*/
Pos find_largest_cluster()
{
	bool temp[12][17] = {0};
	int max = 1, current;
	Pos largest(0,0), curAnchor;
	for(int r = 1; r <=10; ++r)
		for(int c = 1; c<= 15; ++c)
		{
			if (board[r][c] == '#')
				continue;
			curAnchor = Pos(r,c);
			current = find_cluster_size(temp, Pos(r,c), board[r][c], curAnchor);
			if(current > max)
			{
				max = current;
				largest = curAnchor;
			}
			else if(max == current)
			{
				//first we check if the new position is farther left
				if(curAnchor.col < largest.col)
					largest = curAnchor;

				//if we have the same column we need to check the row
				else if(largest.col == curAnchor.col)
					if(curAnchor.row > largest.row)
						largest.row = curAnchor.row;
			}
		
		}
	return largest;
}

/*
	This function deals with compressing the board, displaying output and updating the score.
	Returns true when the current game is over and false otherwise.
*/
bool compress_board(Pos &anchor)
{
	if(!anchor.row)
	{
		cout << "Final score: " << score << ", with " << ballsRemaining << " balls remaining.\n";
		return true;
	}
		
	bool toDelete[12][17] = {0};
	/*
		we know the anchor of the largest cluster, so we use our find_largest_cluster
		function to mark the connected squares for deletion.  Then we iterate through
		the board (valid play positions only) and, if a particular square is marked
		for deletion, we replace the value at that square with '#'
	*/
	char color = board[anchor.row][anchor.col];
	int ballsRemoved = find_cluster_size(toDelete, anchor, board[anchor.row][anchor.col], anchor);
	int scoreFromMove = (ballsRemoved - 2) * (ballsRemoved - 2);
	ballsRemaining -= ballsRemoved;
	score += scoreFromMove;
	
	for(int r = 1; r <= 10; ++r)
		for(int c = 1; c <= 15; ++c)
			if(toDelete[r][c])
				board[r][c] = '#';
	
	//shift balls down - we might as well remember which columns are empty while we do this, for the next step.
	vector<string> ballsInCols;
	for(int c = 1; c <= 15; ++c)
	{
		string curCol = "";
		for(int r = 10; r >= 1; --r)
			if(board[r][c] != '#')
			{
				curCol += board[r][c];
				//we can also clear the board as we go, so it's empty before we refresh the board with
				//our new ball positions
				board[r][c] = '#';
			}			
		if(curCol.length())
			ballsInCols.push_back(curCol);
	}
	//refresh the board with our new ball positions
	for(int c = 0; c < ballsInCols.size(); ++c)
		for(int r = 10, s = 0; s < ballsInCols[c].length() ; --r, ++s)
			board[r][c + 1] = ballsInCols[c][s];
	
	cout << "Move " << moveNum++ << " at (" << -(anchor.row - 11) << ',' << anchor.col
		<< "): removed " << ballsRemoved << " balls of color " << color
		<< ", got " << scoreFromMove << " points.\n";
	
			
	//if the board is empty we get a bonus
	if(ballsRemaining == 0)
	{
		score += 1000;
		cout << "Final score: " << score << ", with " << ballsRemaining << " balls remaining.\n";
		return true;
	}
	return false;
}
int main()
{
	int numGames;
	Pos anchor;
	//pad the board with blank squares
	//pad a row on top and bottom
	for(int i = 0; i < 17; ++i)
		board[i][0] = board[i][11] = '#';
	//pad a column on the left and right
	for(int i = 0; i < 12; ++i)
		board[0][i] = board[16][i] = '#';
		
	cin >> numGames;
	for(int game = 1; game <= numGames; ++game)
	{
		cout << "Game " << game << ":\n\n";
		score = 0, moveNum = 1, ballsRemaining = 150;
		//input starting board
		for(int r = 1; r <= 10; ++r)
			for(int c = 1; c <= 15; ++c)			
				cin >> board[r][c];
	
		do
			anchor = find_largest_cluster();
		while(!compress_board(anchor));
		
		if(game != numGames)
			cout << endl;
	}

	return 0;
}
