/*
	Note: compile using C++11.
	General approach: Use a map from phone number (as a string) to number of occurrences (as an int) to keep track of how many times
	each number appears.  Since the map is ordered by key, iterating through it will give us the phone numbers in lexicographic order.
	
	I convert each input number (in string form, as a combination of letters, hyphens, and numbers) into the proper format, ignoring hyphens,
	mapping letters to numbers using table driven code (see the MAPPING array), adding numbers as is, and inserting a hyphen after the first
	three digits as a final step.  This is done in the getNumber function, which returns the number in the format ###-####, where each #
	is a numerical digit.  I then add to the number of occurrences of that number in the count map (count[getNumber(currNum)]++;).
	For the output, I just iterate through the count map with a C++11 foreach loop and print all the duplicates followed by  the number
	of times they occur, or if there are no duplicates, I print "No duplicates.".
	*/

#include <iostream>
#include <cctype>
#include <map>

using namespace std; 

const char MAPPING[] = {'2','2','2', '3','3','3', '4','4','4', '5','5','5', '6','6','6', '7','\0','7','7', '8','8','8', '9','9','9'};

string getNumber(string s);

int main()
{
	int dataSets, numbers;
	string currNum;

	cin >> dataSets;
	
	for(int i = 0; i < dataSets; i++)
	{
		map<string,int> count;
		cin >> numbers;
		for(int j = 0; j < numbers; j++)
		{
			cin >> currNum;
			count[getNumber(currNum)]++;
		}
		bool duplicates = false;
		for(auto number : count)
			if(number.second > 1)
			{
				duplicates = true;
				cout << number.first << ' ' << number.second << endl;
			}
		if(!duplicates)
			cout << "No duplicates.\n";
		if(i+1 != dataSets) cout << endl;
	}
	return 0;
}

string getNumber(string s)
{
	string result = "";
	for(size_t i = 0; i < s.length(); i++)
	{
		if(s[i] == '-')	//if it's a hyphen, ignore it
			continue;
		if(isalpha(s[i])) //if it's a letter, map it to a number and add it to the string
			result += MAPPING[(int)s[i] - 65];
		else //otherwise it must be a number so we can just add it to the string
			result += s[i];
	}
	return result.insert(3, "-");
}