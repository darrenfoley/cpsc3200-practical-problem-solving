/*
  General Approach: Sort all the points on each wall, then find the midpoints
  between them.  Then go through all the midpoints, checking for intersection
  of the line between each midpoint and the treasure with each other line.
  We keep track of the minimum number of intersections then ouput MIN+1, since
  we also need to break through the outer wall.
 */

/*
 * Line Intersection
 *
 * Author: Howard Cheng
 * Reference:
 *   CLRS, "Introduction to Algorithms", 2nd edition, pages 936-939.
 *
 * Given two lines specified by their endpoints (a1, a2) and (b1, b2),
 * returns true if they intersect, and false otherwise.  The intersection
 * point is not known.
 *
 */

#include <iostream>
#include <cmath>
#include <utility>
#include <climits>
#include <vector>
#include <algorithm>

using namespace std;

/* how close to call equal */
const double EPSILON = 1E-8;

struct Point {
   Point() {}
   Point(double _x, double _y) : x(_x), y(_y) {}
   double x, y; 
};

typedef pair<Point,Point> Wall;

vector<Wall> walls;
vector<Point> lWall;
vector<Point> rWall;
vector<Point> tWall;
vector<Point> bWall;
vector<Point> midpoints;

Point treasure;

double direction(Point p1, Point p2, Point p3)
{
   double x1 = p3.x - p1.x;
   double y1 = p3.y - p1.y;
   double x2 = p2.x - p1.x;
   double y2 = p2.y - p1.y;
   return x1*y2 - x2*y1;
}

istream& operator>>(istream &in, Point &p)
{
   double x, y;
   
   in >> p.x >> p.y;

   return in;
}

int on_segment(Point p1, Point p2, Point p3)
{
   return ((p1.x <= p3.x && p3.x <= p2.x) || (p2.x <= p3.x && p3.x <= p1.x)) &&
      ((p1.y <= p3.y && p3.y <= p2.y) || (p2.y <= p3.y && p3.y <= p1.y));
}

int intersect(Point a1, Point a2, Point b1, Point b2)
{
   double d1 = direction(b1, b2, a1);
   double d2 = direction(b1, b2, a2);
   double d3 = direction(a1, a2, b1);
   double d4 = direction(a1, a2, b2);

   if (((d1 > EPSILON && d2 < -EPSILON) || (d1 < -EPSILON && d2 > EPSILON)) &&
       ((d3 > EPSILON && d4 < -EPSILON) || (d3 < -EPSILON && d4 > EPSILON))) {
      return 1;
   } else {
      return (fabs(d1) < EPSILON && on_segment(b1, b2, a1)) ||
	 (fabs(d2) < EPSILON && on_segment(b1, b2, a2)) ||
	 (fabs(d3) < EPSILON && on_segment(a1, a2, b1)) ||
	 (fabs(d4) < EPSILON && on_segment(a1, a2, b2));
   }
}

bool sideWallCompare(const Point &a, const Point &b)
{
   return a.y < b.y;
}

bool topBotWallCompare(const Point &a, const Point &b)
{
   return a.x < b.x;
}

void addPoint(const Point &a)
{
   //left wall
   if(a.x == 0)
   {
      //corners already added so don't add again
      if(a.y == 100 or a.y == 0)
	 return;
      
      lWall.push_back(a);
   }
   //right wall
   else if(a.x == 100)
   {
      //corners were already added so don't add again
      if(a.y == 100 or a.y == 0)
	 return;

      rWall.push_back(a);
   }
   //bottom wall
   else if(a.y == 0)
      bWall.push_back(a);
      
   //top wall
   else
      tWall.push_back(a);
}

Point getMidpoint(const Point &a, const Point &b)
{
   return Point((a.x+b.x)/2.0, (a.y+b.y)/2.0);
}

void add_midpoints(const vector<Point> & w)
{
   for(int i = 0; i < w.size()-1; ++i)
      midpoints.push_back(getMidpoint(w[i],w[i+1]));
}

void solve()
{
   int numWalls;
   
   Point a, b;
   cin >> numWalls;
   
   walls.clear();
   lWall.clear(); rWall.clear(); tWall.clear(); bWall.clear();
   midpoints.clear();
   lWall.push_back(Point(0,0)); lWall.push_back(Point(0,100));
   rWall.push_back(Point(100,0)); rWall.push_back(Point(100,100));
   tWall.push_back(Point(0,100)); tWall.push_back(Point(100,100));
   bWall.push_back(Point(0,0)); bWall.push_back(Point(100,0));
 
   for(int i = 0; i < numWalls; ++i)
   {
      cin >> a >> b;
      walls.push_back(make_pair(a,b));

      addPoint(a); addPoint(b);
   }
   cin >> treasure;

   sort(lWall.begin(), lWall.end(), sideWallCompare);
   sort(rWall.begin(), rWall.end(), sideWallCompare);
   sort(tWall.begin(), tWall.end(), topBotWallCompare);
   sort(bWall.begin(), bWall.end(), topBotWallCompare);

   add_midpoints(lWall); add_midpoints(rWall);
   add_midpoints(tWall); add_midpoints(bWall);
 
   int MIN = INT_MAX;
   for(int i = 0; i < midpoints.size(); ++i)
   {
      int count = 0;
      for(int j = 0; j < walls.size(); ++j)
      {
	 count += intersect(midpoints[i], treasure, walls[j].first,
			    walls[j].second);
      }
      if(count < MIN)
	 MIN = count;
   }
   cout << "Number of doors = " << MIN+1 << endl;
}

int main()
{
   int numCases;
   cin >> numCases;

   for(int i = 0; i < numCases; ++i)
   {
      solve();
      if(i != numCases -1)
	 cout << endl;
   }
   return 0;
}
