/*
	General Approach: This problem can be solved using the Chinese Remainder Theorem, which
	lets us find a number z, which satisfies 
	z ~ p mod P,
	z ~ e mod E, and
	z ~ i mod I
	(I'm using ~ to mean congruent here)
	where P = 23 , E = 28, and I = 33.
	
	For this problem, z is the next peak (or at least congruent to it mod (23 * 28 * 33).
	p, e, and i are the offsets of known physical, emotional, and intellectual peaks respectively
	from the given day d.
	P, E, and I are the periods of the physical, emotional, and intellectual cycles respectively.
	
	The Chinese Remainder Theorem allows us to solve for z with the formula
	z = B1X1C1 + B2X2C2 + B3X3C3,
	where:
	B1 = E * I, B2 = P * I, and B3 = P * E;
	C1 = p, C2 = e, C3 = i (the remainder part of each congruence); and
	since the Chinese remainder theorem also states that Bi*Xi ~ 1 (mod bi), where bi is
	the modulus part of each of the above congruences (P, E, and I), we can rearrange this
	to get 1/Bi ~ Xi (mod bi).  We can then solve for the modular inverse of Xi using the 
	Extended Euclidean Algorithm. Since the same 3 modular inverses were common to every case,
	I chose to do the Extended Euclidean algorithm on paper to solve for them instead of using Howard's code.
	This allowed me to use constants for the calculations instead of recalculating the same thing several times
	(or even once and storing them), saving me some execution time (and giving me some practice at doing the extended
	algorithm on paper, which I had never done before).
	Anyway, the equations to solve for each Xi in the remainder theorem formula are as follows for this problem:
	X1 ~ 1/B1 (mod P),
	X2 ~ 1/B2 (mod E), and
	X3 ~ 1/B3 (mod I).
	Since each Bi is also constant for all cases in this problem, I stored each Bi * Xi as a single constant to be
	used in calculations.
	
	It also turns out that there are several values of z which satisfy the original congruences, where each
	value n is congruent to z mod (P * E * I).
	With this in mind, once a value for z is found, it is adjusted to meet the problem specifications (z must
	occur AFTER the given day d).  The way I set things up where z is actually the number of days past d, it
	must be the case that 0 < z < 21252.
*/

#include <iostream>

using namespace std ;

//uses Chinese remainder method
int find_peak(int c1, int c2, int c3)
{
	int z;
	const int b1x1 = 5544, b2x2 = 14421, b3x3 = 1288;
	z = b1x1*c1 + b2x2*c2 + b3x3*c3;
	//make sure it's the smallest positive congruence
	if(z > 0)
		z %= 21252;
	while (z <= 0)
		z += 21252;
	return z;
}

int main()
{
	int p, e, i, d, caseNo = 1;
	
	cin >> p >> e >> i >> d;
	while(p != -1 || e != -1 || i != -1 || d != -1)
	{
		cout << "Case " << caseNo++ << ": the next triple peak occurs in "
			<< find_peak(p-d,e-d,i-d) << " days." << endl;
		cin >> p >> e >> i >> d;
	}

	return 0;
}