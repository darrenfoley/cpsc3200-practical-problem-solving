/*
	Note: Compile using C++11
	
	General Approach: I use breadth-first search to generate what can be plugged into
	each receptacle with/without adapters.
	
	I then model each case as a graph which can be solved using a Max Flow algorithm
	and use Howard's code to solve this.  Then, the min number of devices left unplugged
	is equal to number of devices - max flow.
*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <unordered_map>
#include <list>
#include <cassert>

using namespace std;

unsigned int _edgeIndex_ = 2; //start at 2 as 0,1 are reserved for s,t
const static unsigned int MAXNODES = 5000;

struct Edge;
typedef list<Edge>::iterator EdgeIter;

struct Edge {
  int to;
  int cap;
  int flow;
  bool is_real;
  EdgeIter partner;
  
  Edge(int t, int c, bool real = true)
    : to(t), cap(c), flow(0), is_real(real)
  {};

  int residual() const
  {
    return cap - flow;
  }
};

struct Graph {
  list<Edge> *nbr;
  int num_nodes;
  Graph(int n)
    : num_nodes(n)
  {
    nbr = new list<Edge>[num_nodes];
  }

  ~Graph()
  {
    delete[] nbr;
  }

  // note: this routine adds an edge to the graph with the specified capacity,
  // as well as a residual edge.  There is no check on duplicate edge, so it
  // is possible to add multiple edges (and residual edges) between two
  // vertices
  void add_edge(int u, int v, int cap)
  {
    nbr[u].push_front(Edge(v, cap));
    nbr[v].push_front(Edge(u, 0, false));
    nbr[v].begin()->partner = nbr[u].begin();
    nbr[u].begin()->partner = nbr[v].begin();
  }
};

void push_path(Graph &G, int s, int t, const vector<EdgeIter> &path, int flow)
{
  for (int i = 0; s != t; i++) {
    if (path[i]->is_real) {
      path[i]->flow += flow;
      path[i]->partner->cap += flow;
    } else {
      path[i]->cap -= flow;
      path[i]->partner->flow -= flow;
    }
    s = path[i]->to;
  }
}

// the path is stored in a peculiar way for efficiency: path[i] is the
// i-th edge taken in the path.
int augmenting_path(const Graph &G, int s, int t, vector<EdgeIter> &path,
		    vector<bool> &visited, int step = 0)
{
  if (s == t) {
    return -1;
  }
  for (EdgeIter it = G.nbr[s].begin(); it != G.nbr[s].end(); ++it) {
    int v = it->to;
    if (it->residual() > 0 && !visited[v]) {
      path[step] = it;
      visited[v] = true;
      int flow = augmenting_path(G, v, t, path, visited, step+1);
      if (flow == -1) {
	return it->residual();
      } else if (flow > 0) {
	return min(flow, it->residual());
      }
    }
  }
  return 0;
}

// note that the graph is modified
int network_flow(Graph &G, int s, int t)
{
  vector<bool> visited(G.num_nodes);
  vector<EdgeIter> path(G.num_nodes);
  int flow = 0, f;

  do {
    fill(visited.begin(), visited.end(), false);
    if ((f = augmenting_path(G, s, t, path, visited)) > 0) {
      push_path(G, s, t, path, f);
      flow += f;
    }
  } while (f > 0);
  
  return flow;
}

//generates a map from receptacle to a vector of plugs which can be adapted to fit it
void generate_adapter_possibilities(unordered_map<string, vector<string> > &adapters)
{
	for(auto &i : adapters)
	{
		int count = 0;
		auto size = i.second.size();
		unordered_map<string,string> seen;
		for(auto j : i.second)
			seen[j] = j;
		while(true)
		{
			if(count == size)
				break;
			string temp = i.second[count];	
			if(temp != i.first)
			{
				string current;
				for(int j = 0; j < adapters[temp].size(); j++)
				{
					current = adapters[temp][j];
					//if we haven't seen it, add it and update seen
					if(seen.find(current) == seen.end())
					{
						i.second.push_back(current);
						seen[current] = current;
						size++;
					}
				}
			}
			count++;
		}
	}
}

//models the current case as a flow graph and solves for the max flow using Howard's network flow code
int generate_graph_and_find_max_flow(const vector<string> &receptacles, unordered_map<string, vector<string> > &adapters, const vector<string> &devices)
{
	Graph graph(MAXNODES) ;
	unordered_map<string,vector<int> > mapping;
	for(auto r : receptacles)
	{
		//add the receptacle to the graph and remember it's index so it can be the parent of nodes which can adapt into it
		int currentParent = _edgeIndex_;
		graph.add_edge(_edgeIndex_++, 1, 1);
		//add each type of plug that can attach to this parent to the graph and remember the mapping from char to int for future reference
		for(auto a : adapters[r])
		{
			graph.add_edge(_edgeIndex_, currentParent, 1);
			mapping[a].push_back(_edgeIndex_++);
		}	
	}
	int count = 0;
	//now we deal with adding the devices to the graph
	for(auto d : devices)
	{
		//connect s to each device with capacity 1
		graph.add_edge(0, _edgeIndex_, 1);
		
		//then we connect each device to the receptacles (we are actually connecting many of them to adapters, but don't worry about that)
		for(auto m : mapping[d])
			graph.add_edge(_edgeIndex_, m, 1);
		
		_edgeIndex_++;
	}
	return network_flow(graph, 0, 1);
}

int main()
{
	int numCases, numReceptacles, numDevices, numAdapters;
	string garbage;
	string receptacle, devicePlug, adapterReceptacle, adapterPlug;
	cin >> numCases;
	for(int i = 0; i < numCases; i++)
	{
		vector<string> devices, receptacles;
		unordered_map<string, vector<string> > adapters;
		//take receptacle input
		cin >> numReceptacles;
		for(int j = 0; j < numReceptacles; j++)
		{
			cin >> receptacle;
			receptacles.push_back(receptacle);
			/*
				if adapters does not already contain the current receptacle,
				add as both key and value - will simplify things later
				*/
			if(adapters.find(receptacle) == adapters.end())
				adapters[receptacle].push_back(receptacle);
		}

		//take devices input
		cin >> numDevices;
		for(int j = 0; j < numDevices; j++)
		{
			cin >> garbage >> devicePlug;
			devices.push_back(devicePlug);
		}

		//take adapters input
		cin >> numAdapters;
		for(int j = 0; j < numAdapters; j++)
		{
			cin >> adapterReceptacle >> adapterPlug;
			adapters[adapterPlug].push_back(adapterReceptacle);
		}

		generate_adapter_possibilities(adapters);
		cout << numDevices - generate_graph_and_find_max_flow(receptacles, adapters, devices) ;
		_edgeIndex_ = 2;
		if(i != numCases - 1)
			cout << endl ;
		cout << endl;
	}
}