/*
  General approach: This is a dynamic programming problem since we will end up
  in the same state several times and we don't want to recalculate things.
  In this problem state refers to where you are (which lake) and how much time
  you have left in minutes.

  We get the following recurrence:

  maxFish(lake, time <= 0) = 0
  
  maxFish(last lake, time) = the amount of fish we catch by spending the
  remaining time at this lake, keeping in mind that once
  f[last lake] - d[last lake] <= 0 we just let the time run out and don't fish
  (it doesn't make sense to fish for <= 0 fish).  This can be calculated with a
  simple for loop.

  (I'll use "stay(i)" to mean stay at the current lake for i minutes and fish,
  since it's a little difficult to notate what is happening)
  maxFish(lake, time) = max( (stay(0) + maxFish(lake + 1, time - travel time)),
  (stay(5) + maxFish(lake + 1, time - 5 - travel time)), ... ,
  (stay(time)) ).

  I use the prev array to remember how long I should stay at each lake in each
  situation.  Then at the output stage I can iterate through it lake by lake,
  with time starting out as the total time in minutes, then updated to be
  time = time - prev[lake][time] - travel time.

  Keep in mind that whenever I use time as an index it has been divided by 5,
  since all times in this problem are divisible by 5.
*/

#include <iostream>
#include <algorithm>

using namespace std;

//192 = 16 * 60 / 5 -- no point in storing numbers not divisible by 5
int DP[25][192]; //the dynamic programming table indexed by lake, time remaining
int prev[25][192]; //the previous array, indexed by lake, time remaining
int t[24];
int f[25];
int d[25];
int n;

//returns the maximum expected fish starting at a given lake with a given time
//remaining
int maxFish(int lake, int time)
{
   if(time <= 0)
      return 0;
   
   if(DP[lake][time/5] != -1)
      return DP[lake][time/5];

   if(lake == n-1)
   {
      int fish = 0;
      for(int i = 0; i < time / 5; ++i)
      {
	 int temp = f[lake] - i* d[lake];
	 if(temp <= 0)
	 {
	    prev[lake][time/5] = i*5;
	    return DP[lake][time/5] = fish;
	 }
	 fish += temp;
      }
      prev[lake][time/5] = time;
      return DP[lake][time/5] = fish;
   }

   //we try staying at the current lake for various 5 minute intervals,
   //from 0 mins to time mins and keep track of which decision yields the most
   //expected fish
   int m = 0, mt, recFish, curFish = 0;
   for(int i = 0; i <= time/5; ++i)
   {
      /*
	rec fish will be:
	max fish from going to the next lake with time - (time spent at this
	lake) - time spent driving between this lake and the next lake
      */
      recFish = maxFish( lake + 1, time - 5*i - 5*t[lake] );

      //tie-breaking case
      if(recFish + curFish == m)
	 mt = i*5;

      else if(recFish + curFish > m)
      {
	 m = recFish + curFish;
	 mt = i*5;
      }

      //there's no point in staying longer if we won't catch more fish
      int temp = f[lake] - i*d[lake];
      if(temp > 0)      
	 curFish += temp;
   }

   prev[lake][time/5] = mt;

   return DP[lake][time/5] = m;
}

int main()
{
   int h;

   cin >> n;
   while(n != 0)
   {
      fill(&DP[0][0], &DP[0][0] + sizeof(DP)/sizeof(int), -1);
      cin >> h;

      for(int i = 0; i < n; ++i)
	 cin >> f[i];
      for(int i = 0; i < n; ++i)
	 cin >> d[i];
      for(int i = 0; i < n-1; ++i)
	 cin >> t[i];

      int expectedFish = maxFish(0, h*60);

      //output
      int time = h*60 / 5;
      for(int lake = 0; lake < n; ++lake)
      {
	 if(time*5 <= 0)
	    cout << 0;
	 else
	 {
	    cout << prev[lake][time];
	    time = time - (prev[lake][time] / 5) - t[lake];
	 }

	 if(lake != n - 1)
	    cout << ", ";
	 else
	    cout << endl;
      }

      cout << "Number of fish expected: " << expectedFish << endl;
      
      cin >> n;
      if(n != 0)
	 cout << endl;
   }
   return 0;
}
