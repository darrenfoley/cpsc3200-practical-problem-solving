/*
  General Approach: Use delta arrays and a for loop to look at the 8 tiles
  (up,down,right,left, and diagonals) touching each tile, counting how many
  of these are mines.  This is, of course, only performed around tiles which
  themselves are not mines.  Tiles which are out of bounds are ignored.
*/
#include <iostream>
#include <algorithm>

using namespace std;

const static int DROW[] = {-1,0,1,-1,1,-1,0,1};
const static int DCOL[] = {-1,-1,-1,0,0,1,1,1};

void get_input_field(int n, int m, char field[][100]);
void solve_field(int n, int m, char field[][100]);
char solve_square(int n, int m, int row, int col, char field[][100]);
void print_field(int n, int m, char field[][100]);

int main()
{   
   int n, m, fieldNo = 1;

   cin >> n >> m;

   while(1)
   {
      char field[100][100];
      
      get_input_field(n, m, field);

      cout << "Field #" << fieldNo << ":\n";  

      solve_field(n, m, field);
      print_field(n, m, field);
      
      cin >> n >> m;
      if(n == 0 && m == 0)
	 break;
      cout << endl ;
      fieldNo++;
   }
}

void get_input_field(int n, int m, char field[][100])
{
   for(int row = 0; row < n; row++)
      for(int col = 0; col < m; col++)
	 cin >> field[row][col];
}

void solve_field(int n, int m, char field[][100])
{
   char current ;
   
   for(int row = 0; row < n; row++)
      for(int col = 0; col < m; col++)
      {
	 if((current = field[row][col]) == '*')
	    continue;
	 field[row][col] = solve_square(n, m, row, col, field);
      }
}

char solve_square(int n, int m, int row, int col, char field[][100])
{
   int count = 0;
   int curRow;
   int curCol;
   for(int i = 0; i < 8; i++)
   {
      curRow = row + DROW[i];
      curCol = col + DCOL[i];
      
      if((curRow >= 0) && (curCol >= 0) && (curRow < n) && (curCol < m))
	 if (field[curRow][curCol] == '*')
	    count++;
   }
   return (char)(count + '0');
}

void print_field(int n, int m, char field[][100])
{
   for(int row = 0; row < n; row++)
   {
      for(int col = 0; col < m; col++)
	 cout << field[row][col];
      cout << endl ;
   }
}
