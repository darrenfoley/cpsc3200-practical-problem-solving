/*
  General Approach: Fingerings are stored in a map where the key is the note and
  the value is a bool array indicating which fingers are used for that note.
  We also keep a bool array indicating whether a given finger is currently pressed.
  To figure out if a finger is pressed I simply "NOT" whether it was previously pressed
  and "AND" this with whether the note requires it to be pressed.  If it is a new press,
  this check will return true, which can be interpretted as a 1 and added to the total for
  that finger.  If it is false, it will be interpretted as a 0, which will not change
  the total when added.
*/

#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <string>

using namespace std;

/*--globals--*/
//fingerings for each note
const static bool c[] = {0,1,1,1,0,0,1,1,1,1};
const static bool d[] = {0,1,1,1,0,0,1,1,1,0};
const static bool e[] = {0,1,1,1,0,0,1,1,0,0};
const static bool f[] = {0,1,1,1,0,0,1,0,0,0};
const static bool g[] = {0,1,1,1,0,0,0,0,0,0};
const static bool a[] = {0,1,1,0,0,0,0,0,0,0};
const static bool b[] = {0,1,0,0,0,0,0,0,0,0};
const static bool C[] = {0,0,1,0,0,0,0,0,0,0};
const static bool D[] = {1,1,1,1,0,0,1,1,1,0};
const static bool E[] = {1,1,1,1,0,0,1,1,0,0};
const static bool F[] = {1,1,1,1,0,0,1,0,0,0};
const static bool G[] = {1,1,1,1,0,0,0,0,0,0};
const static bool A[] = {1,1,1,0,0,0,0,0,0,0};
const static bool B[] = {1,1,0,0,0,0,0,0,0,0};

unordered_map<char,const bool*> fingerings;

int main()
{
   int t;
   string song;
   //load the fingerings into the map
   fingerings['c'] = c;
   fingerings['d'] = d;
   fingerings['e'] = e;
   fingerings['f'] = f;
   fingerings['g'] = g;
   fingerings['a'] = a;
   fingerings['b'] = b;
   fingerings['C'] = C;
   fingerings['D'] = D;
   fingerings['E'] = E;
   fingerings['F'] = F;
   fingerings['G'] = G;
   fingerings['A'] = A;
   fingerings['B'] = B;

   cin >> t;
   cin.ignore();

   for(int i = 0; i < t; i++)
   {
      bool pressed[10] = {false};
      int count[10] = {0};

      getline(cin, song);

      for(int beat = 0; beat < song.length(); beat++)
      {
	 for(int finger = 0; finger < 10; finger++)
	 {
	    count[finger] += !pressed[finger] && fingerings[song[beat]][finger];
	    pressed[finger] = fingerings[song[beat]][finger];
	 }
      }
      for(int finger = 0; finger < 10; finger++)
	 finger == 9 ? cout << count[finger] : cout << count[finger] << ' ';
      
      cout << endl ;
   }
}
