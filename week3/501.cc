/*
  General Approach:
  Keep 2 priority queues, 1 min and 1 max.  Make sure the max queue always
  has i elements in it, so that the ith sorted element is always at the top.
  Keep the remaining (u(x) - i) elements in the min queue.  When inserting an
  element into the min queue, first check that it is larger than the top
  of the max queue.  If it is, put it in the min queue, otherwise, put it in
  the max queue, and move the top of the max queue to the min queue.
*/

#include <iostream>
#include <queue>
#include <vector>

using namespace std;

int main()
{
   int m, n, temp, numDataSets;
   

   cin >> numDataSets;
   for(int curDataSet = 0; curDataSet < numDataSets; ++curDataSet)
   {
      vector <int> a;
      priority_queue<int, vector<int>, less<int> > max;
      priority_queue<int, vector<int>, greater<int> > min;
      int i = 0, uPrev = 0, uCur;
      cin >> m >> n;

      for(int j = 0; j < m; ++j)
      {
	 cin >> temp;
	 a.push_back(temp);
      }

      for(int j = 0; j < n; j++)
      {
	 //update i
	 ++i;
	 //get the current value of u
	 cin >> uCur;

	 //make sure there are i elements in the max queue
	 if(min.size() > 0)
	 {
	    max.push(min.top());
	    min.pop();
	 }
	 else
	    max.push(a[uPrev]);

      
	 //make sure ((current value of u) - i) elements are in the min queue
	 //if the element to be inserted is less than the top of the max
	 //queue, put the top in the min queue and the current element
	 //in the min queue
	 for(int k = max.size() + min.size(); k < uCur; k++)
	 {
	    temp = max.top();
	    if(a[k] < temp)
	    {
	       max.pop();
	       max.push(a[k]);
	       min.push(temp);
	    }
	    else
	       min.push(a[k]);
	 }
      
	 //update uPrev
	 uPrev = uCur;

	 //output
	 cout << max.top() << endl;
      }
      if(curDataSet < numDataSets - 1)
	 cout << endl;
   }

      
   return 0;
}
