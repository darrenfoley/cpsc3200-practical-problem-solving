/*
  General Approach:
  For each case, create a UnionFind object of size number of computers + 1
  (I make it 1 bigger and just don't use 0 to simplify my code).
  Each time a connection is made, merge nodes i and j
  To check if two computers i and j are interconnected, we simply check
  if find(i) == find(j) is true.  Each time this check is true, we increase
  the number of successes; otherwise we increase the number of failures.
*/
#include <iostream>
#include <sstream>
using namespace std;

// UnionFind class -- based on Howard Cheng's C code for UnionFind
// Modified to use C++ by Rex Forsyth, Oct 22, 2003
//
// Constuctor -- builds a UnionFind object of size n and initializes it
// find -- return index of x in the UnionFind
// merge -- updates relationship between x and y in the UnionFind


class UnionFind
{
      struct UF { int p; int rank; };

   public:
      UnionFind(int n) {          // constructor
	 howMany = n;
	 uf = new UF[howMany];
	 for (int i = 0; i < howMany; i++) {
	    uf[i].p = i;
	    uf[i].rank = 0;
	 }
      }

      ~UnionFind() {
         delete[] uf;
      }

      int find(int x) { return find(uf,x); }        // for client use
      
      bool merge(int x, int y) {
	 int res1, res2;
	 res1 = find(uf, x);
	 res2 = find(uf, y);
	 if (res1 != res2) {
	    if (uf[res1].rank > uf[res2].rank) {
	       uf[res2].p = res1;
	    }
	    else {
	       uf[res1].p = res2;
	       if (uf[res1].rank == uf[res2].rank) {
		  uf[res2].rank++;
	       }
	    }
	    return true;
	 }
	 return false;
      }
      
   private:
      int howMany;
      UF* uf;

      int find(UF uf[], int x) {     // recursive funcion for internal use
	 if (uf[x].p != x) {
	    uf[x].p = find(uf, uf[x].p);
	 }
	 return uf[x].p;
      }
};

int main()
{
   int numCases, numComputers, i, j;
   char pairType;
   string line;
   cin >> numCases;
   for(int caseNo = 0; caseNo < numCases; ++caseNo)
   {
      cin >> numComputers;
      UnionFind uf(numComputers+1); // make it 1 bigger and don't use 0 so that we dont need to adjust indices
      int successful = 0, unsuccessful = 0;
      cin.ignore();
      while(getline(cin,line))
      {
	 if(line.length() == 0)
	    break;
	 istringstream iss(line);
	 iss >> pairType >> i >> j;
	 switch(pairType)
	 {
	    case 'c':
	       uf.merge(i, j);
	       break;
	    case 'q':
	       uf.find(i) == uf.find(j) ? ++successful : ++unsuccessful;
	       break;
	 }
      }
      cout << successful << ',' << unsuccessful << endl;
      if(caseNo != numCases -1)
	 cout << endl ;
   }
   return 0;
}
