/*
  General Approach:
  Store receipts in a multiset, in which values will be sorted in non-decreasing
  order.  At the end of each day, get the biggest receipt (at iterator
  --receipts.end()) and subtract from it the smallest receipt (at iterator
  receipts.begin()), then call receipts.erase() on both iterators to remove
  them from the set.

  Keep a running total of amount paid for each case and print it at the end.
  The total needs to be stored in a long long, since we are adding at most
  10^6 to the total each day, over up to 5000 days (5 000 000 000), which
  is greater than 2^32 - 1.
*/
#include <iostream>
#include <set>

using namespace std;

int main()
{
   int numDays, k, curReceipt;

   cin >> numDays;
   while(numDays != 0)
   {
      long long totalPaid = 0;
      multiset<int> receipts;
      for(int day = 0; day < numDays; ++day)
      {
	 cin >> k;
	 for(int receiptNo = 0; receiptNo < k; ++receiptNo)
	 {
	    cin >> curReceipt;
	    receipts.insert(curReceipt);
	 }
	 totalPaid += *(--receipts.end()) - *(receipts.begin());
	 receipts.erase(receipts.begin());
	 receipts.erase(--receipts.end());
      }
      cout << totalPaid << endl;
      cin >> numDays; 
   }
   return 0;
}
