/*
 * Recursive routine for generating all permutaions
 * with a place for adding a "if it makes sense" check to allow early pruning
 */

#include <algorithm>
void search(int perm, int n, int index = 0)
{
   //base case
   if (index == n)
   {
      //do something
      return;
   }

   for(int i = index; i < n; ++i)
   {
      //make the swap
      swap(perm[i], perm[index]);

      if(/* perm[index] makes sense */)
	 search(perm, n, index + 1);
      
      //then swap back (backtrack)
      swap(perm[i], perm[index]);
   }
}
