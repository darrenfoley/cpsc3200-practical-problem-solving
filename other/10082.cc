#include <iostream>

using namespace std;

const string keyboard = "`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./";

bool is_leap_year(int year)
{
   int x = year % 4;
   int y = year % 100;
   int z = year % 400;
   return (!x && y) || !z;
}

int main()
{
   string str;

   while(getline(cin,str))
   {
      string result = "";
      
      for(int i = 0; i < str.length(); ++i)
      {
	 result += str[i] == ' ' ? ' ' : keyboard[keyboard.find(str[i])-1];
      }
      cout << result << endl;
   }

   return 0;
}
