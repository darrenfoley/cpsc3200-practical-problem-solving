#include <set>
#include <iostream>

using namespace std;

int main()
{
   int arr[] = {1,1,2,3,2,7,2,5,4,6,9,9,8,6,0};
   //initialize using range constructor
   set<int> aSet(arr, arr+15);

   for(auto i : aSet)
      cout << i << ' ';
   cout << endl;
   return 0;

}
