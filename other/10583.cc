//UVA 10583 - Ubiquitous Religious

#include <iostream>
#include <unordered_set>

using namespace std;
// UnionFind class -- based on Howard Cheng's C code for UnionFind
// Modified to use C++ by Rex Forsyth, Oct 22, 2003
//
// Constuctor -- builds a UnionFind object of size n and initializes it
// find -- return index of x in the UnionFind
// merge -- updates relationship between x and y in the UnionFind


class UnionFind
{
      struct UF { int p; int rank; };

   public:
      UnionFind(int n) {          // constructor
	 howMany = n;
	 uf = new UF[howMany];
	 for (int i = 0; i < howMany; i++) {
	    uf[i].p = i;
	    uf[i].rank = 0;
	 }
      }

      ~UnionFind() {
         delete[] uf;
      }

      int find(int x) { return find(uf,x); }        // for client use
      
      bool merge(int x, int y) {
	 int res1, res2;
	 res1 = find(uf, x);
	 res2 = find(uf, y);
	 if (res1 != res2) {
	    if (uf[res1].rank > uf[res2].rank) {
	       uf[res2].p = res1;
	    }
	    else {
	       uf[res1].p = res2;
	       if (uf[res1].rank == uf[res2].rank) {
		  uf[res2].rank++;
	       }
	    }
	    return true;
	 }
	 return false;
      }
      
   private:
      int howMany;
      UF* uf;

      int find(UF uf[], int x) {     // recursive funcion for internal use
	 if (uf[x].p != x) {
	    uf[x].p = find(uf, uf[x].p);
	 }
	 return uf[x].p;
      }
};


int main()
{
   int n, m, a, b, caseNo = 1;
 
   cin >> n >> m;

   while(n != 0 or m != 0)
   {
      UnionFind uf(n);
      unordered_set<int> aSet;
      
      for(int i = 0; i < m ; ++i)
      {
	 cin >> a >> b;
	 a -= 1,b -=1;
	 uf.merge(a,b);
      }
      for(int i = 0; i < n; ++i)
	 aSet.insert(uf.find(i));
      cout << "Case " << caseNo++ << ": " << aSet.size() << endl;
      cin >> n >> m;
   }
   return 0;
}
