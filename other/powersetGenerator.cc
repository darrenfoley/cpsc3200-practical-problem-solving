#include <bitset>
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

void generate_powersets(const vector<int> &vec)
{
   // bitset<32> mask;
   cout << "The powerset of {";
   for(auto it : vec)
      cout << it << ' ';
   cout << "\b} is:" << endl << '{';
   
   for(int i = 0; i < pow(2,vec.size()); ++i)
   {
      bitset<32> mask(i);
      cout << '{';
      for(int j = 0; j < vec.size(); ++j)
	 if(mask[j])
	    cout << vec[j] << ' ';
      if(i)
	 cout << '\b';
      if(i == pow(2,vec.size()) - 1)
	 cout << "}}";
      else cout << "}, ";
   }
   
   cout << endl;

}

int main()
{
   int numCases, n, temp;
   vector<int> vec;
   cin >> numCases;

   for(int i = 0; i < numCases; ++i)
   {
      cin >> n;
      for(int j = 0; j < n; ++j)
      {
	 cin >> temp;
	 vec.push_back(temp);
      }
      generate_powersets(vec);
   }

   return 0;

}
