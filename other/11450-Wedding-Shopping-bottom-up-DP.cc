#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

int prices[25][25];
bool table[20][201];

int main()
{
   /*
     N = num test cases
     M = available money
     C = num garments we have to buy
     K = num diff models for each garment
     we actually store k in prices[garment type][0]
    */
   int N, M, C;

   cin >> N;
   for(int caseNo = 0; caseNo < N; ++caseNo)
   {
      memset(table, 0, sizeof(table));
      cin >> M >> C;
      for(int type = 0; type < C; ++type)
      {
	 cin >> prices[type][0];
	 for(int i = 1; i <= prices[type][0]; ++i)
	    cin >> prices[type][i];
      }
      //init table for reachable garments of the first type
      for(int i = 1; i <= prices[0][0]; ++i)
	 if(M - prices[0][i] >= 0)
	    table[0][M-prices[0][i]] = true;

      for(int type = 1; type < C; ++type)
      {
	 for(int money = 0; money < M; ++money)
	 {
	    //if it's possible to end up with money Money after chosing the
	    //type-1 garment type, then we need to fill the table for the other
	    //garments starting with this amount of money
	    if(table[type-1][money])
	       for(int i = 1; i <= prices[type][0]; ++i)
	       {
		  if(money - prices[type][i] >= 0)
		     table[type][money-prices[type][i]] = true;
	       }
	 }
      }
      //our DP table is now filled

      /*
	answer found in last row when g = c - 1. find the lowest amount of money
	that is reachable for the last garment
      */
      int min = -1;
      for(int i = 0; i <= M - C; ++i)
      {
	 if(table[C-1][i])
	 {
	    min = i;
	    break;
	 }
      }
      if(min == -1)
	 cout << "no solution";
      else
	 cout << M - min;
      cout << endl;
   }
   return 0;
}
