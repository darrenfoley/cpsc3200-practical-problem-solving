#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;
long long books[500];
int k, m;

bool all_books_copied(long long maxPages)
{
   int index = 0;
   long long cur;
   
   for(int i = 0; i < k; ++i)
   {
      cur = maxPages;
      while(true)
      {
	 if(index == m)
	    return true;
	 if(cur - books[index] < 0)
	    break;
	 else
	    cur -= books[index++];
      }
   }
   return false;
}

void solve()
{
   long long hi = 5000000000, lo = 0, mid;
   while(hi-1 != lo)
   {
      mid = (hi + lo) / 2;

      if(all_books_copied(mid))
	 hi = mid;
      else
	 lo = mid;
   }

   //hi, lo are both equal to the max pages now
   int index = m-1;
   string answer="";
   long long cur;
   for(int i = 0; i < k; ++i)
   {
      cur = hi;
      do
      {
	 if(cur - books[index] < 0)
	    break;
	 cur -= books[index];
	 
	 if(answer.length() != 0)
	    answer = " " + answer;
	 
	 answer = to_string(books[index--]) + answer;
      }
      while((index + 1) > k - 1 - i);

      if(i != k -1)
	 answer = "/ " + answer;
   }
   cout << answer << endl;
}

int main()
{
   int N, temp;

   cin >> N;
   for(int i = 0; i < N; ++i)
   {
      cin >> m >> k;

      for(int book = 0; book < m; ++book)
	 cin >> books[book];

      solve();
   }
   
   return 0;
}
