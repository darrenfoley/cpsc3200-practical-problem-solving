//10107 - What is the Median?
//written by Darren Foley - Feb 9/15

#include <iostream>
#include <queue>
#include <vector>

using namespace std;

int main()
{
   int x;
   priority_queue<int, vector<int>, less<int> > max;
   priority_queue<int, vector<int>, greater<int> > min;
   
   while (cin >> x)
   {
      //insert into appropriate queue
      //first check for cases where 1 or both queues are empty

      //both are empty
      if(max.empty() and min.empty())
	 max.push(x);

      //only min is empty (it can never happen that only max is empty)
      else if(min.empty())
      {
	 if(x < max.top())
	 {
	    min.push(max.top());
	    max.pop();
	    max.push(x);
	 }
	 else
	    min.push(x);
      }

      //neither is empty
      else if(x < max.top())
	 max.push(x);
      else
	 min.push(x);

      //adjust sizes of queues
      
      //if max has more than 1 more than min, move some into max
      while(min.size() + 1 < max.size())
      {
	 min.push(max.top());
	 max.pop();
      }

      //if min has more than max
      while(min.size() > max.size())
      {
	 max.push(min.top());
	 min.pop();
      }

      //output
      
      //if queues are equal in size
      if(max.size() == min.size())
	 cout << (max.top() + min.top())/2 << endl;
      else
	 cout << max.top() << endl;
   }

   return 0;
}
