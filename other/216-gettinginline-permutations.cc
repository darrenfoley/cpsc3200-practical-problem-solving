#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>
#include <cfloat> //DBL_MAX
#include <math.h>
#include <iomanip>
using namespace std;

bool compare(pair<int,int> a, pair<int,int> b)
{
   if(a.first < b.first or (a.first == b.first and a.second < b.second))
      return true;
   return false;
}

template <class T>
ostream & printpair(pair<T,T> p, ostream &os)
{
   os << '(' << p.first << ',' << p.second << ')';
   return os;
}

void solve(vector<pair<int,int> > network)
{
   double min = DBL_MAX, cur;
   vector<pair<int,int> > minvec;
   do
   {
      cur = 0;
      for(int i = 0; i < network.size() - 1; ++i)
      {
	 cur += hypot(network[i].first - network[i+1].first,
		      network[i].second - network[i+1].second) + 16.0;
	 if(cur > min)
	    goto hell;
      }
      if(cur < min)
      {
	 min = cur;
	 minvec = network;
      }
     hell: ;
   }while(next_permutation(network.begin(), network.end()));

   cout << fixed << setprecision(2);
   for(int i = 0; i < minvec.size() - 1; ++i)
   {
      cout << "Cable requirement to connect ";
      printpair(minvec[i], cout) << " to ";
      printpair(minvec[i+1], cout) << " is ";

      cout << hypot(minvec[i].first - minvec[i+1].first,
		    minvec[i].second - minvec[i+1].second) + 16.0 << " feet.\n";
   }
   cout << "Number of feet of cable required is " << min << ".\n";
}


int main()
{
   int networkNum = 1, n, x, y;
   string stars = "**********************************************************";
 
   cin >> n;

   while(n != 0)
   {
      vector<pair<int,int> > network;
      cout << stars << endl << "Network #" << networkNum++ << endl;

      for(int i = 0; i < n; ++i)
      {
	 cin >> x >> y;
	 network.push_back(make_pair(x,y));
      }

      sort(network.begin(),network.end(),compare);

      solve(network);
      cin >> n;
   }
   return 0;
}
