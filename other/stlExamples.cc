#include <iostream>
#include <algorithm>
#include <numeric>
using namespace std;

int mult(int a, int b) {return a * b;}

int main()
{
   int arr[10];
   int arr2[5];
   fill(arr, arr+10, 57);
   cout << "57 count = " << count(arr,arr+10,57) << endl << endl;
   for(int i = 0; i < 10; ++i)
   {
      cout << arr[i] << ' ';
      arr[i] += i;
   }
   cout << endl << endl ;
   cout << "max: " << *max_element(arr,arr+10) << endl << "min: " << *min_element(arr,arr+10) << endl << endl;
   
   cout << "59 found at: "<< find(arr,arr+10,59) << endl << endl;
   
   swap(arr[0],arr[9]);
   
   for(int i = 0; i < 10; ++i)
      cout << arr[i] << ' ';

   cout << endl << endl;

   sort(arr,arr+10);

   for(int i = 0; i < 10; ++i)
      cout << arr[i] << ' ';
   cout << endl << endl;

   copy(arr+5, arr+10, arr2);
   for(int i = 0; i < 5; ++i)
      cout << arr2[i] << ' ';
   cout << endl << endl;

   //lower bound returns iterator pointing to first element that is greater
   //than or equal to val - must be sorted
   cout << "lb test: " <<*lower_bound(arr,arr+10, 59) << endl << endl;

   //upper bound returns iterator pointing to first elemetn that is greater
   //than val - must be sorted
   cout << "ub test: " << *upper_bound(arr,arr+10, 59) << endl << endl;

   //equal range returns a pair which are interators pointing to the beginning
   //and ending of a range that is equal to val - must be sorted
   fill(arr+2,arr+6,10);
   arr[0] = 5;
   sort(arr,arr+10);
   for(int i = 0; i < 10; ++i)
      cout << arr[i] << ' ';
   cout << endl << endl;
   cout << equal_range(arr, arr+10, 10).first << ' ' << equal_range(arr,arr+10,10).second << endl << endl;


   string str = "12345";

   while(next_permutation(str.begin(), str.end()))
      cout << str <<  ' ';
   cout << endl << endl;

   reverse(str.begin(),str.end());
   
   while(prev_permutation(str.begin(),str.end()))
      cout << str << ' ';
   cout << endl << endl;

   cout << accumulate(arr,arr+10, 0) << endl << endl; //include numeric
   cout << accumulate(arr,arr+10, 1, mult) << endl << endl; //include numeric

   partial_sum(arr2,arr2+5, arr2); // can add another multiply
   cout << "partial sum: ";
   for(int i = 0; i < 5; ++i)
      cout << arr2[i] << ' ';
   cout << endl << endl;

   for(int i = 0; i < 5; ++i)
      arr2[i] = i;

   adjacent_difference(arr2, arr2+5, arr2);
   cout << "adjacent_difference: ";
   for(int i = 0; i < 5; ++i)
      cout << arr2[i] << ' ';
   cout << endl << endl;
   return 0;
}
