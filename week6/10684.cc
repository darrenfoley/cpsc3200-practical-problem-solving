/*
  General Approach: Keep track of the current maximum streak (I just store
  the total sum in an int).
  At each step, update the current streak by adding the next bet.  If the
  result is less than 0, continue to the next iteration.  Otherwise check
  if current streak > max streak and if it is, set max streak = current
  streak.
 */

#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
   int length;
   
   cin >> length;
   
   while(length != 0)
   {
      int maxStreak = 0, curStreak = 0, curBet;
      
      for(int bet = 0; bet < length; ++bet)
      {
	 cin >> curBet;
	
	 if((curStreak + curBet) < 0)
	    curStreak = 0;
	 else
	    curStreak += curBet;
	 
	 maxStreak = max(curStreak, maxStreak);
      }
      
      if(maxStreak)
	 cout << "The maximum winning streak is " << maxStreak << ".\n";
      else
	 cout << "Losing streak.\n" ;
      
      cin >> length;
   }
}
