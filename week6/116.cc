/*
  General Approach: I solved this dynamic programming problem
  with a top down approach.  So I recurse through the board
  from left to right, but the work is done from right to left,
  which is ideal, since as work is completed I store the previous
  row from right to left, which I can then trace from left to
  right in the output.  
 */

#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;

int numRows, numCols;
int matrix[10][100];
int table[10][100];
int previous[10][100];
int dr[3];

int solve_rec(int, int);

//wrapper function
void solve()
{
   int start = INT_MAX;
   int minLength = INT_MAX, tempLength;
   
   for(int row = 0; row < numRows; ++row)
   {
      tempLength = solve_rec(row, 0);

      if(tempLength < minLength)
      {
	 minLength = tempLength;
	 start = row;
      }
      else if(tempLength == minLength)
	 start = min(row, start);
   }

   int row = start;
   cout << start + 1;
   if (numCols != 1)
      cout << ' ';
   
   for(int col = 0; col < numCols - 1; ++col)
   {
      cout << previous[row][col] + 1;
      if(col != numCols - 2)
      {
	 cout << ' ';
	 row = previous[row][col];
      }
   }
   cout << endl;
   cout << minLength << endl;
}

//recursive function
int solve_rec(int row, int col)
{
   //check the dynamic programming array to see if we have already computed it
   if(table[row][col] != INT_MAX)
      return table[row][col];
   //if we've reached the end of the board
   if(col == numCols - 1)
      return table[row][col] = matrix[row][col];
   
   int chosenRow = INT_MAX, minLength = INT_MAX, tempLength;

   for(int i = 0; i < 3; ++i)
   {
      tempLength = solve_rec((row + dr[i]) % numRows, col + 1);

      if(tempLength < minLength)
      {
	 minLength = tempLength;
	 chosenRow = (row + dr[i]) % numRows;
      }
      else if(tempLength == minLength)
	 chosenRow = min((row+dr[i])%numRows,chosenRow);
   }

   previous[row][col] = chosenRow;
   
   return table[row][col] = minLength + matrix[row][col] ;
}

int main()
{
   while(cin >> numRows >> numCols)
   {
      for(int row = 0; row < numRows; ++row)
      {
	 for(int col = 0; col < numCols; ++col)
	    cin >> matrix[row][col];
	 fill(table[row], table[row] + numCols, INT_MAX);
      }
      dr[0] = numRows - 1; dr[1] = 0; dr[2] = 1;
      solve();
   }
   
   return 0;
}
