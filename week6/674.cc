/*
  General Approach: We set up the recurrance
  Base case:
  f(0,k) = f(t,0) = 1
  
  if (largest allowed denomination (denom[k]) > t):
  f(t,k) = f(t,k-1)
  
  else:
  f(t,k) = f(t-denom[k],k) + f(t,k-1)

  where f(t,k) is numWays(t,k) in my program.
  
  Then we simply set up a 2D dynamic programming array
  (numCombos) to store values which we have already computed
  and check if each input to numWays has been computed
  before computing it and simply return it if it has.
 */

#include <iostream>
#include <algorithm>

using namespace std;

const int denom[] = { 1, 5, 10, 25, 50 };
int numCombos[7490][5];

int numWays(int t, int k)
{
   if(numCombos[t][k] != -1) return numCombos[t][k];

   //if the biggest allowed denom is bigger than t, start with the next biggest
   if(denom[k] > t)
      return numCombos[t][k] = numWays(t, k-1);

   return numCombos[t][k] = numWays(t-denom[k], k) + numWays(t, k-1); 
}

int main()
{
   int amount ;
   
   //set up the table
   for(int row = 1; row < 7490; ++row)
      fill(numCombos[row] + 1, numCombos[row] + 5, -1);
   fill(numCombos[0], numCombos[0] + 5, 1);
   for(int i = 0; i < 7490; ++i)
      numCombos[i][0] = 1;

   while(cin >> amount)
      cout << numWays(amount, 4) << endl;
   
   return 0;
}
