/*
  General Approach:
  At each step, we can only hit a missile if it has a higher
  altitude than the previous missile we hit, so in order to
  maximize the number of missiles we hit, we need to find
  the longest strictly ascending subsequence.  I use Howard's
  code for this.
 */

/*
 * Longest Ascending Subsequence
 *
 * Author: Howard Cheng
 * Reference:
 *   Gries, D.  The Science of Programming
 *
 * Given an array of size n, asc_seq returns the length of the longest
 * ascending subsequence, as well as one of the subsequences in S.
 * sasc_seq returns the length of the longest strictly ascending
 * subsequence.  It runs in O(n log n) time.
 *
 * Also included are simplified versions when only the length is needed.
 *
 * Note: If we want to find do the same things with descending
 * subsequences, just reverse the array before calling the routines.
 * 
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <cassert>

using namespace std;

int sasc_seq(vector<int> A, int n, int S[])
{
  vector<int> last(n+1), pos(n+1), pred(n);
  if (n == 0) {
    return 0;
  }

  int len = 1;
  last[1] = A[pos[1] = 0];

  for (int i = 1; i < n; i++) {
    int j = lower_bound(last.begin()+1, last.begin()+len+1, A[i]) -
      last.begin();
    pred[i] = (j-1 > 0) ? pos[j-1] : -1;
    last[j] = A[pos[j] = i];
    len = max(len, j);
  }

  int start = pos[len];
  for (int i = len-1; i >= 0; i--) {
    S[i] = A[start];
    start = pred[start];
  }

  return len;
}

int main(void)
{
   int numCases, numHits;
   string str;

   cin >> numCases;
   cin.ignore(); cin.ignore();
   
   for(int i = 0; i < numCases; ++i)
   {
      vector<int> altitudes;
      do
      {
	 getline(cin, str);
	 if(str.length() != 0)
	    altitudes.push_back(atoi(str.c_str()));	 
      }while(str.length() != 0);
      
      int * LIS = new int[altitudes.size()];
      numHits = sasc_seq(altitudes, altitudes.size(), LIS);

      cout << "Max hits: " << numHits << endl;
      for(int j = 0; j < numHits; ++j)
	 cout << LIS[j] << endl;

      delete LIS;

      if(i != numCases - 1) cout << endl; //blank line between cases
   }
  
  return 0;
}
