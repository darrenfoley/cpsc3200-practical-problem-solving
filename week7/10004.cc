/*
  General Approach: DFS traversal of the graph, colouring as we go.
  At each step, we look at the current node's neighbours.
  If they haven't been coloured, we attempt to colour them (and their
  neighbours recursively) and return false if this fails.
  If they have been coloured we check that they are coloured differently than
  the current node, if not we return false.  If we make it through all the
  neighbours for a given call we return true for that call.

  bool adjmatrix[][] is the adjacency matrix for the graph.
  
  int colour[] acts as a sort of visited array which also stores the colour of
  a given node as an int (0 or 1).  A value of -1 in this array signifies we
  have not visited the node yet.
 */
#include <iostream>
#include <algorithm>

using namespace std;

int N, L;

bool adjmatrix[200][200];
int colour[200];

bool can2Colour(int v=0, int currColour=0)
{
   colour[v] = currColour;

   for(int i = 0; i < N; ++i)
   {
      //if it's a neighbour
      if(adjmatrix[v][i])
      {
	 //if it hasnt been coloured
	 if(colour[i] == -1)
	 {
	    //recurse, colouring it the opposite of curr and return false if it
	    //fails
	    if(!can2Colour(i,1-currColour))
	       return false;
	 }
	 //otherwise if it has been coloured but it has the same colour as
	 //the current node, then return false
	 else if(currColour == colour[i])
	    return false;
      }
   }
   //2-colouring was successful so return true
   return true;
}

int main()
{
   int a, b;
   
   cin >> N >> L;

   while(N != 0)
   {
      //initialize the arrays
      for(int i = 0; i < N; ++i)
	 fill(adjmatrix[i], adjmatrix[i] + 200,false);
      fill(colour, colour + N, -1);

      //get the input
      for(int i = 0; i < L; ++i)
      {
	 cin >> a >> b;
	 adjmatrix[a][b] = adjmatrix[b][a] = true;
      }

      //output
      if(!can2Colour())
	 cout << "NOT ";
      cout << "BICOLORABLE.\n";

      cin >> N >> L;
   }

   return 0;
}
