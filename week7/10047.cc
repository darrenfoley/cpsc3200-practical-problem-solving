/*
  General Approach: BFS of states(row,column,direction,colour) until
  either we get to T with green (0) on the ground or we run out of
  states to check.  Remember states we have already been in (visited)
  and do not visit them more than once, so that we do not end up in a
  cycle.
  This is an implied graph problem, since we can always figure out our
  neighbours given the current state (node).
 */

#include <iostream>
#include <algorithm>
#include <queue>
#include <cstring> //memset

using namespace std;

//0,1,2,3
//N,E,S,W 
int dr[4] = {-1,0,1,0} ;
int dc[4] = {0,1,0,-1} ;

char board[27][27];
int vis[27][27][4][5];

struct state
{
   state(int r, int c, int d, int clr) : row(r), col(c), dir(d), colour(clr) {}
   int row;
   int col;
   int dir;
   int colour;
};

int solve(int sRow, int sCol)
{
   queue<state> q;
   q.push(state(sRow,sCol,0,0));
   
   vis[sRow][sCol][0][0] = 0;
   
   while(!q.empty())
   {
      state cur = q.front(); q.pop();
      
      //if we're on an obstacle
      if(board[cur.row][cur.col] == '#')
	 continue;

      int curTime = vis[cur.row][cur.col][cur.dir][cur.colour];
      
      //if we made it to the end with the right colour return the time
      if(board[cur.row][cur.col] == 'T' and cur.colour == 0)
	 return curTime;

      //otherwise try all possible moves from the current state, adding them to
      //the queue

      //try moving forward
      if(vis[cur.row + dr[cur.dir]][cur.col + dc[cur.dir]][cur.dir][(cur.colour + 1) % 5] == -1)
      {
	 q.push(state(cur.row + dr[cur.dir], cur.col + dc[cur.dir], cur.dir, (cur.colour + 1) % 5));
	 vis[cur.row + dr[cur.dir]][cur.col + dc[cur.dir]][cur.dir][(cur.colour + 1) % 5] = curTime + 1;
      }
      
      //try turning left
      if(vis[cur.row][cur.col][(cur.dir + 3) % 4][cur.colour] == -1)
      {
	 q.push(state(cur.row, cur.col, (cur.dir + 3) % 4, cur.colour)) ;
	 vis[cur.row][cur.col][(cur.dir + 3) % 4][cur.colour] = curTime + 1;
      }
      
      //try turning right
      if(vis[cur.row][cur.col][(cur.dir + 1) % 4][cur.colour] == -1)
      {
	 q.push(state(cur.row, cur.col, (cur.dir + 1) % 4, cur.colour));
	 vis[cur.row][cur.col][(cur.dir + 1) % 4][cur.colour] = curTime + 1;
      }
   }
   return -1;
}


int main()
{
   int M, N, sRow, sCol, ans, caseNo = 1;
   char temp;
   cin >> M >> N;
   
   while(true)
   {
      for(int row = 0; row <= M + 1; ++row)
	 fill(board[row], board[row] + N + 2, '#');
      //14580 = 27*27*4*5
      memset(vis, -1, sizeof(int) * 14580);
      
      for(int row = 1; row <= M; ++row)
	 for(int col = 1; col <= N; ++col)
	 {
	    cin >> temp ;
	    if(temp == 'S')
	    {
	       sRow = row;
	       sCol = col;
	       board[row][col] = '.';
	    }
	    else board[row][col] = temp;
	 }

      ans = solve(sRow, sCol);

      cout << "Case #" << caseNo++ << endl;
	 if(ans != -1)
	    cout << "minimum time = " << ans << " sec\n";
	 else
	    cout << "destination not reachable\n";

	 cin >> M >> N;

	 if(M != 0 or N != 0)
	    cout << endl;
	 else
	    break;
      }
      return 0;
   }
