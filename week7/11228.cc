/*
  General Approach: Use Kruskal's algorithm to find a minimum spanning
  tree of cities.  This will the the minimum total length of
  road/railroad which will connect all of the cities.  We need to
  modify the algorithm a little bit to add to the number of railroads
  each time we select an edge of length greater than r.  We need this
  since the number of states is equal to the number of railroads + 1.
  Another modification is that we need to keep track of 2 other
  variables: the total length of road and the total length of
  railroad and update them appropriately as we build the MST.
 */

/*
 * Implementation of Kruskal's Minimum Spanning Tree Algorithm
 *
 * Author: Howard Cheng
 *
 * This is a routine to find the minimum spanning tree.  It takes as
 * input:
 *
 *      n: number of vertices
 *      m: number of edges
 *  elist: an array of edges (if (u,v) is in the list, there is no need
 *         for (v,u) to be in, but it wouldn't hurt, as long as the weights
 *         are the same).
 *
 * The following are returned:
 *
 *  index: an array of indices that shows which edges from elist are in
 *         the minimum spanning tree.  It is assumed that its size is at
 *         least n-1.
 *   size: the number of edges selected in "index".  If this is not
 *         n-1, the graph is not connected and we have a "minimum
 *         spanning forest."
 *
 * The weight of the MST is returned as the function return value.
 * 
 * The run time of the algorithm is O(m log m).
 *
 * Note: the elements of elist may be reordered.
 *
 * Modified by Rex Forsyth using C++  Aug 28, 2003
 * This version defines the unionfind and edge as classes and  provides
 * constructors. The edge class overloads the < operator. So the sort does
 * not use a  * cmp function. It uses dynamic arrays.
 */

#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cassert>
#include <algorithm>
#include <utility>
#include <vector>
#include <math.h>

using namespace std;

int r, numRailroads;

class UnionFind
{
   struct UF { int p; int rank; };

public:
   UnionFind(int n) {          // constructor
      howMany = n;
      uf = new UF[howMany];
      for (int i = 0; i < howMany; i++) {
	 uf[i].p = i;
	 uf[i].rank = 0;
      }
   }

   ~UnionFind() {
      delete[] uf;
   }

   int find(int x) { return find(uf,x); }        // for client use
      
   bool merge(int x, int y) {
      int res1, res2;
      res1 = find(uf, x);
      res2 = find(uf, y);
      if (res1 != res2) {
	 if (uf[res1].rank > uf[res2].rank) {
	    uf[res2].p = res1;
	 }
	 else {
	    uf[res1].p = res2;
	    if (uf[res1].rank == uf[res2].rank) {
	       uf[res2].rank++;
	    }
	 }
	 return true;
      }
      return false;
   }
      
private:
   int howMany;
   UF* uf;

   int find(UF uf[], int x) {             // for internal use
      if (uf[x].p != x) {
	 uf[x].p = find(uf, uf[x].p);
      }
      return uf[x].p;
   }
};

class Edge {

public:
   Edge(int i=-1, int j=-1, double weight=0) {
      v1 = i;
      v2 = j;
      w = weight;
   }
   bool operator<(const Edge& e) const { return w < e.w; }

   int v1, v2;          /* two endpoints of edge                */
   double w;            /* weight, can be double instead of int */
};


pair<double,double> mst(int n, int m, Edge elist[])
{
   UnionFind uf(n);

   sort(elist, elist+m);

   double rrLength= 0, roadLength = 0;
   
   for (int i = 0; i < m; i++) {
      int c1 = uf.find(elist[i].v1);
      int c2 = uf.find(elist[i].v2);
      if (c1 != c2) {
	 if (elist[i].w > r)
	 {
	    rrLength += elist[i].w;
	    ++numRailroads;
	 }
	 else
	    roadLength += elist[i].w;
	 
	 uf.merge(c1, c2);
      }
   }

   return make_pair(roadLength,rrLength);
}

int main()
{
   int numCases, n, a, b;

   cin >> numCases;

   for(int caseNo = 1; caseNo <= numCases; ++caseNo)
   {
      numRailroads = 0;
      cin >> n >> r;
      vector<pair<int,int> > xy;
      for(int i = 0; i < n; ++i)
      {
	 cin >> a >> b;
	 xy.push_back(make_pair(a,b));
      }

      Edge * elist = new Edge[n*n];
      int k = 0;
      for (int i = 0; i < n; i++) 
	 for (int j = i+1; j < n; j++) 
	    elist[k++] = Edge(i,j,hypot(xy[i].first - xy[j].first,
					xy[i].second - xy[j].second));

      pair<double,double> extensions = mst(n,k, elist);
      
//output
      cout << "Case #" << caseNo << ": " << numRailroads + 1 << ' '
	   << round(extensions.first) << ' ' << round(extensions.second) << endl;
//cleanup
      delete [] elist;
   }

   return 0;
}
